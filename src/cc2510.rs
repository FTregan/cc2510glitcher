use core::ops::RangeInclusive;
use core::slice::Iter;

use cortex_m::delay::Delay;
use embedded_hal::digital::v2::OutputPin;
use pio::{Instruction, InstructionOperands};
use rp2040_hal::gpio::{
    FunctionNull, FunctionPio0, FunctionSioOutput, Pin, PinState, PullDown, ValidFunction
};
use rp2040_hal::pio::ShiftDirection::Left;
use rp2040_hal::pio::{
    PIOBuilder, PinDir, Running, Rx, StateMachine, Tx, UninitStateMachine, PIO, PIO0SM0, PIO0SM1, PIO0SM2
};
use rp_pico::pac::PIO0;

use crate::{debug_instr_1, debug_instr_2, debug_instr_3, wr_config, CHIP_ERASE, GET_PC, INC_DPTR, LJMP, MOVX_ATDPTR_A, MOVX_A_ATDPTR, MOV_A_CONST, MOV_DPTR_CONST, NOP, RD_CONFIG, READ_STATUS, RESUME};

use self::commands::Command;

const ENTER_DEBUG_IRQ_MASK: u8 = 0b00000001;
const CPU_HALTED_MASK: u8 = 0b0010_0000;
const CHIP_ERASE_DONE_MASK: u8 = 0b1000_0000;

#[rustfmt::skip]
pub mod instructions {

    /// a CC2510 instruction defined by the first byte of its opcode and its length.
    pub struct Instruction<const LENGTH:usize>(pub u32);

    pub const CLR_BIT         : Instruction<2> = Instruction(0xC2);
    pub const NOP             : Instruction<1> = Instruction(0x00);
    pub const MOV_A_DIRECT    : Instruction<2> = Instruction(0xE5);
    pub const MOV_DIRECT_A    : Instruction<2> = Instruction(0xF5);
    pub const MOV_A_CONST     : Instruction<2> = Instruction(0x74);
    pub const MOV_DPTR_CONST  : Instruction<3> = Instruction(0x90);
    pub const MOVX_A_ATDPTR   : Instruction<1> = Instruction(0xE0);
    pub const MOVX_ATDPTR_A   : Instruction<1> = Instruction(0xF0);
    pub const INC_DPTR        : Instruction<1> = Instruction(0xA3);
    pub const LJMP            : Instruction<3> = Instruction(0x02);
    pub const SJMP            : Instruction<2> = Instruction(0x80);
}

#[rustfmt::skip]
pub mod commands {
    use super::instructions::*;

    pub struct Command(pub u32, pub u32, pub u32);

    pub const CHIP_ERASE        : Command = Command(08, 08, 0x14000000);
    pub const GET_CHIP_ID       : Command = Command(08, 16, 0x68000000);
    pub const HALT              : Command = Command(08, 08, 0x44000000);
    pub const RESUME            : Command = Command(08, 08, 0x4c000000);
    pub const READ_STATUS       : Command = Command(08, 08, 0x34000000);
    pub const RD_CONFIG         : Command = Command(08, 08, 0x24000000);
    pub const GET_PC            : Command = Command(08, 16, 0x28000000);

    pub const fn debug_instr_1(instruction: Instruction<1>) -> Command {
        Command(16, 08, (0x55 << 24) | ((instruction.0 as u32) << 16))
    }
    pub const fn debug_instr_2(instruction: Instruction<2>, operand: u8) -> Command {
        Command(24, 08, (0x56 << 24) | ((instruction.0 as u32) << 16) | ((operand as u32) << 8))
    }
    pub const fn debug_instr_3(instruction: Instruction<3>, operand: u16) -> Command {
        Command(32, 08, (0x57 << 24) | ((instruction.0 as u32) << 16) | (operand as u32))
    }
    pub const fn wr_config(c: u8) -> Command {
        Command(16, 08, 0x1d000000 | ((c as u32) << 16))
    }
}

pub struct Cc2510<
    ResetPin: ValidFunction<FunctionSioOutput>,
    PowerPin: ValidFunction<FunctionSioOutput>,
> {
    pio: PIO<PIO0>,
    delay: Delay,
    power_off_delay_ms: u32,
    power_on_delay_ms: u32,
    reset_on_delay_us: u32,
    reset_off_delay_us: u32,
    glitch_state_machine: StateMachine<PIO0SM2, Running>,
    send_command_rx: Rx<PIO0SM1>,
    send_command_tx: Tx<PIO0SM1>,
    glitch_tx: Tx<PIO0SM2>,
    reset_pin: Pin<ResetPin, FunctionSioOutput, PullDown>,
    power_pin: Pin<PowerPin, FunctionSioOutput, PullDown>,
}

pub struct plop {
    titi: u32,
    toto: u32,
    tata: u16,
    tutu: u16,
    tyty: u16
}

impl<
        ResetPin: ValidFunction<FunctionSioOutput>,
        PowerPin: ValidFunction<FunctionSioOutput>,
    > Cc2510<ResetPin, PowerPin>
{

    pub fn new<
        OscilloscopePin: ValidFunction<FunctionPio0>,
        GlitchPin: ValidFunction<FunctionPio0>,
        DataPin: ValidFunction<FunctionPio0>,
        ClockPin: ValidFunction<FunctionPio0>,
    >(
        mut pio: PIO<PIO0>,
        delay: Delay,
        power_off_delay_ms: u32,
        power_on_delay_ms: u32,
        reset_on_delay_us: u32,
        reset_off_delay_us: u32,
        enter_debug_state_machine: UninitStateMachine<PIO0SM0>,
        send_command_state_machine: UninitStateMachine<PIO0SM1>,
        glitch_state_machine: UninitStateMachine<PIO0SM2>,
        glitch_pin: Pin<GlitchPin, FunctionNull, PullDown>,
        oscilloscope_pin: Pin<OscilloscopePin, FunctionNull, PullDown>,
        power_pin: Pin<PowerPin, FunctionNull, PullDown>,
        data_pin: Pin<DataPin, FunctionNull, PullDown>,
        clock_pin: Pin<ClockPin, FunctionNull, PullDown>,
        reset_pin: Pin<ResetPin, FunctionNull, PullDown>,
    ) -> Self {
        let glitch_pin = glitch_pin.into_function();
        let _ = oscilloscope_pin.into_function::<FunctionPio0>();
        let data_pin = data_pin.into_function();
        let power_pin = power_pin.into_push_pull_output_in_state(PinState::Low);
        let clock_pin = clock_pin.into_function();
        let reset_pin = reset_pin.into_push_pull_output_in_state(PinState::Low);

        Self::init_enter_debug_state_machine(&mut pio, enter_debug_state_machine, clock_pin.id().num);

        let (send_command_rx, send_command_tx) = Self::init_send_command_state_machine(
            &mut pio,
            send_command_state_machine,
            clock_pin.id().num,
            data_pin.id().num,
        );

        let (glitch_state_machine, glitch_tx) =
            Self::init_glitch_state_machine(&mut pio, glitch_state_machine, glitch_pin.id().num);

        let mut cc2510 = Cc2510 {
            pio,
            delay,
            power_off_delay_ms,
            power_on_delay_ms,
            reset_on_delay_us,
            reset_off_delay_us,
            send_command_rx,
            send_command_tx,
            glitch_state_machine,
            glitch_tx,
            power_pin,
            reset_pin,
        };

        cc2510.reinit_debug_session();

        cc2510
    }

    pub fn send(&mut self, c: &Command) -> u32 {
        self.send_with_glitch(c, 0, 0)
    }

    pub fn send_with_glitch(&mut self, c: &Command, delay: u32, duration: u32) -> u32 {
        self.glitch_tx.write(delay);
        self.glitch_tx.write(duration);
        self.send_command_tx.write(c.0);
        self.send_command_tx.write(c.1);
        self.send_command_tx.write(c.2);
        while self.send_command_rx.is_empty() {}

        self.send_command_rx.read().unwrap()
    }

    pub fn reinit_debug_session(&mut self) {
        self.power_cycle();
        self.enter_debug();
    }

    fn power_cycle(&mut self) {
        self.power_off();
        self.power_on();
    }

    fn power_on(&mut self) {
        self.power_core(true);
        let _ = self.power_pin.set_high();
        self.delay.delay_ms(self.power_on_delay_ms);
    }

    pub fn power_off(&mut self) {
        self.power_core(false);
        let _ = self.power_pin.set_low();
        let _ = self.reset_pin.set_low();
        self.delay.delay_ms(self.power_off_delay_ms);
    }

    pub fn refresh_and_read_status(&mut self) -> u8 {
        let _ = self.send(&debug_instr_1(NOP));
        self.send(&READ_STATUS) as u8
    }

    pub fn chip_erase_blocking(&mut self) {
        self.send(&CHIP_ERASE);
        while self.send(&READ_STATUS) as u8 & CHIP_ERASE_DONE_MASK == 0 {}
    }

    pub fn copy_to_ram(
        &mut self,
        start: u16,
        data: Iter<u8>,
    ) {
        let _ = self.send(&debug_instr_3(MOV_DPTR_CONST, start));
        for d in data {
            let _ = self.send(&debug_instr_2(MOV_A_CONST, *d));
            let _ = self.send(&debug_instr_1(MOVX_ATDPTR_A));
            let _ = self.send(&debug_instr_1(INC_DPTR));
        }
    }

    fn power_core(&mut self, on: bool) {
        self
        .glitch_state_machine
        .exec_instruction(
            Instruction {
                operands:
                    InstructionOperands::MOV {
                        destination: (pio::MovDestination::Y),
                        op: (pio::MovOperation::None),
                        source: (pio::MovSource::Y)
                    },
                delay: 0,
                side_set: Some(if on {0b01} else {0b00})
            }
        );
    }

    pub fn erase_first_page_flash(&mut self) {
        const CODE: &[u8] = &[
            0x75, 0xAB, 0x23,    // MOV FWT,    #23h
            0x75, 0xAD, 0x00,    // MOV FADDRH, #00
            0x75, 0xAC, 0x00,    // MOV FADDRL, #00
            0x75, 0xAE, 0x01,    // MOV FCTL,   #01h     ; ERASE
            0xE5, 0xAE,          // MOV A, FCTL
            0x20, 0xE7, 0xFB,    // JB A.7, -5           ; bit 7 of FCTL is BUSY (erase in progress)
        ];
        self.run_code(CODE);
    }

    pub fn flash_test_data(&mut self) {
        const CODE: &[u8] = &[
            0xE5, 0xAE,          // MOV A, FCTL
            0x20, 0xE7, 0xFB,    // JB A.7, -5           ; bit 7 of FCTL is BUSY (erase in progress)
            0x75, 0xAB, 0x23,    // MOV FWT,    #23h
            0x75, 0xAD, 0x00,    // MOV FADDRH, #..h
            0x75, 0xAC, 0x08,    // MOV FADDRL, #..h
            0x75, 0xAE, 0x02,    // MOV FCTL,   #02h     ; start write
            0xE5, 0xAE,          // MOV A, FCTL
            0x20, 0xE6, 0xFB,    // JB A.6, -5           ; bit 6 of FCTL is SWBSY (write in progress)
            0x75, 0xAF, 0xfa,    // MOV FWDATA, #..h
            0x75, 0xAF, 0xb8,    // MOV FWDATA, #..h
            0xE5, 0xAE,          // MOV A, FCTL
            0x20, 0xE6, 0xFB,    // JB A.6, -5           ; bit 6 of FCTL is SWBSY (write in progress)
            0x75, 0xAF, 0x13,    // MOV FWDATA, #..h
            0x75, 0xAF, 0x37,    // MOV FWDATA, #..h
            0xE5, 0xAE,          // MOV A, FCTL
            0x20, 0xE6, 0xFB,    // JB A.6, -5           ; bit 6 of FCTL is SWBSY (write in progress)

            0xa5                 // TRAP
        ];
        self.run_code(CODE);
        while self.send(&READ_STATUS) & 0b1000_0000 == 0 {}
    }

    pub fn lock(&mut self) {
        const CODE: &[u8] = &[
            0xE5, 0xAE,          // MOV A, FCTL
            0x20, 0xE7, 0xFB,    // JB A.7, -5           ; bit 7 of FCTL is BUSY (erase in progress)
            0x75, 0xAB, 0x23,    // MOV FWT,    #23h
            0x75, 0xAD, 0x00,    // MOV FADDRH, #..h
            0x75, 0xAC, 0x00,    // MOV FADDRL, #..h
            0x75, 0xAE, 0x02,    // MOV FCTL,   #02h     ; start write
            0xE5, 0xAE,          // MOV A, FCTL
            0x20, 0xE6, 0xFB,    // JB A.6, -5           ; bit 6 of FCTL is SWBSY (write in progress)
            0x75, 0xAF, 0x00,    // MOV FWDATA, #..h
            0x75, 0xAF, 0x00,    // MOV FWDATA, #..h
            0xE5, 0xAE,          // MOV A, FCTL
            0x20, 0xE6, 0xFB,    // JB A.6, -5           ; bit 6 of FCTL is SWBSY (write in progress)

            0xa5                 // TRAP
        ];
        let previous_configuration = self.send(&RD_CONFIG) as u8;
        self.send(&wr_config(0b0000_1111)); // select Flash Information Page
        self.run_code(CODE);
        self.send(&wr_config(previous_configuration));
    }

    /**
     * todo : only meant to be executed on HALTED cpu only
     *
     * upload the given code at beginning of the SRAM of the cc2510 (XCODE:0xF000) and execute it.
     *
     * todo : The code must end on a `TRAP` instruction (opcode `0xA5`)
     */
    pub fn run_code(
        &mut self,
        code: &[u8],
    ) {
        self.copy_to_ram(0xf000, code.iter());
        self.send(&debug_instr_3(LJMP, 0xf000));
        self.send(&RESUME);
        while self.send(&READ_STATUS) as u8 & CPU_HALTED_MASK == 0  { };
    }

    fn enter_debug(&mut self) {
        let _ = self.reset_pin.set_low();
        self.delay.delay_us(self.reset_on_delay_us);
        self.pio.clear_irq(ENTER_DEBUG_IRQ_MASK); // unhalts the pio program
        while (self.pio.get_irq_raw() & ENTER_DEBUG_IRQ_MASK) == 0 {} // waits for the program to end its loops
        let _ = self.reset_pin.set_high();
        self.delay.delay_us(self.reset_off_delay_us);
    }

    fn init_enter_debug_state_machine(
        pio: &mut PIO<PIO0>,
        sm: UninitStateMachine<PIO0SM0>,
        clock_pin_num: u8,
    ) {
        let p = pio_proc::pio_file!("./src/cc_enter_debug.pio"); //

        let ip = pio.install(&p.program).unwrap();
        let (mut initialised_sm, _, _) = //-
            PIOBuilder::from_program(ip)
            .clock_divisor_fixed_point(1, 0)
            .side_set_pin_base(clock_pin_num)
            .build(sm);
        initialised_sm.set_pindirs([(clock_pin_num, PinDir::Output)]);

        initialised_sm.start();
    }

    fn init_send_command_state_machine(
        pio: &mut PIO<PIO0>,
        sm: UninitStateMachine<PIO0SM1>,
        clock_pin_num: u8,
        data_pin_num: u8,
    ) -> (Rx<PIO0SM1>, Tx<PIO0SM1>) {
        let program = pio_proc::pio_file!("./src/cc_send_command.pio");
        let installed = pio.install(&program.program).unwrap();

        let (mut initialised_sm, rx, tx) = //-
            PIOBuilder::from_program(installed)
            .clock_divisor_fixed_point(32, 0)
            .side_set_pin_base(clock_pin_num)
            .set_pins(data_pin_num, 1)
            .out_pins(data_pin_num, 1)
            .in_pin_base(data_pin_num)
            .pull_threshold(32)
            .autopull(true)
            .autopush(false)
            .in_shift_direction(Left)
            .out_shift_direction(Left)
            .build(sm);
        initialised_sm.set_pindirs([(clock_pin_num, PinDir::Output)]);
        initialised_sm.start();

        (rx, tx)
    }

    fn init_glitch_state_machine(
        pio: &mut PIO<PIO0>,
        sm: UninitStateMachine<PIO0SM2>,
        glitch_pin_num: u8,
    ) -> (StateMachine<PIO0SM2, Running>, Tx<PIO0SM2>) {
        let program = pio_proc::pio_file!("./src/cc_glitch.pio");
        let installed = pio.install(&program.program).unwrap();
        let (mut initialised_sm, _, tx) = //-
            PIOBuilder::from_program(installed)
            .clock_divisor_fixed_point(1, 0)
            .side_set_pin_base(glitch_pin_num)
            .autopull(true)
            .build(sm);
        initialised_sm.set_pindirs([(glitch_pin_num, PinDir::Output)]);
        initialised_sm.set_pindirs([(glitch_pin_num + 1, PinDir::Output)]);
        let running_sm = initialised_sm.start();

        (running_sm, tx)
    }

    pub fn heat_map(
        &mut self,
        delays: RangeInclusive<u32>,
        durations: RangeInclusive<u32>,
        attempts: u16,
        mut f: impl FnMut(HeatMapCallbaclParameter),
    ) {
        while self.send(&READ_STATUS) & 0b100 != 0 {
            let _ = self.send_with_glitch(&debug_instr_3(MOV_DPTR_CONST, 0x0013), 2044, 12);
        }

        for delay in delays {
            for duration in durations.clone() {
                let mut false_success_count: u16 = 0;
                let mut success_count: u16 = 0;
                let mut screwed_count: u16 = 0;

                for _attempt in 1..=attempts {
                    const TEST_VALUE: u8 = 0b11100100;
                    let a = self.send_with_glitch(&debug_instr_2(MOV_A_CONST, TEST_VALUE), delay, duration) as u8;
                    let debug_locked = self.send(&READ_STATUS) & 0b100 != 0;
                    if !debug_locked {
                        if a == TEST_VALUE {
                            success_count += 1;
                        } else {
                            false_success_count += 1;
                        }
                    } else {
                        if a != 0 {
                            self.reinit_debug_session();
                            while self.send(&READ_STATUS) & 0b100 != 0 {
                                let _ = self.send_with_glitch(&debug_instr_3(MOV_DPTR_CONST, 0x0013), 2044, 12);
                            }
                            screwed_count += 1;
                        }
                    }
                }

                f(HeatMapCallbaclParameter {delay, duration, success_count, false_success_count, screwed_count});
            }
        }
    }

    pub fn dump(
        &mut self,
        memory_range: RangeInclusive<u16>,
        delay: u32,
        duration: u32,
        mut f: impl FnMut(u8),
    ) {
        for address in memory_range {
            f(
                loop {
                    let _ = self.send_with_glitch(&debug_instr_3(MOV_DPTR_CONST, address), delay, duration);
                    if self.send(&READ_STATUS) != 0b10110010 {
                        self.reinit_debug_session();
                        continue;
                    }

                    let b = self.send_with_glitch(&debug_instr_1(MOVX_A_ATDPTR), delay, duration) as u8;
                    if self.send(&READ_STATUS) != 0b10110010 {
                        self.reinit_debug_session();
                        continue;
                    }
                    break b;
                }
            );
        }
    }
}

pub struct HeatMapCallbaclParameter {
    pub delay: u32,
    pub duration: u32,
    pub success_count: u16,
    pub false_success_count :u16,
    pub screwed_count: u16,
}
