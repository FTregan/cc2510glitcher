// #![feature(proc_macro_byte_character)]
// #![feature(type_changing_struct_update)]
// #![feature(trace_macros)]
#![no_std]
#![no_main]

mod cc2510;

use core::ops::RangeInclusive;
use core::slice::Iter;

use build_time::build_time_local;
use cc2510::commands::*;
use cc2510::instructions::*;
use cc2510::Cc2510;
use cortex_m::delay::Delay;
use fugit::Rate;
use fugit::RateExtU32;
use hal::clocks::init_clocks_and_plls;
use hal::entry;
use hal::gpio::bank0::Gpio0;
use hal::gpio::bank0::Gpio1;
use hal::gpio::FunctionNull;
use hal::gpio::FunctionUart;
use hal::gpio::Pin;
use hal::gpio::Pins;
use hal::gpio::PullDown;
use hal::pac::UART0;
use hal::prelude::*;
use hal::uart::ValidPinRx;
use hal::uart::ValidPinTx;
use hal::uart::ValidUartPinout;
use hal::uart::{DataBits, StopBits, UartConfig};
use hal::Sio;
use panic_halt as _;
use rp2040_hal::gpio::FunctionSioOutput;
use rp2040_hal::gpio::ValidFunction;
use rp_pico::hal;
use rp_pico::hal::pac;
use rp_pico::hal::uart::UartPeripheral;

const XTAL_FREQ_HZ: u32 = 12_000_000u32;

#[entry]
fn main() -> ! {
    let mut pac = pac::Peripherals::take().unwrap();
    let core = pac::CorePeripherals::take().unwrap();
    let mut watchdog: hal::Watchdog = hal::Watchdog::new(pac.WATCHDOG);
    let clocks = init_clocks_and_plls(
        XTAL_FREQ_HZ,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();
    let sio = Sio::new(pac.SIO);
    let pins = Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let frequency = clocks.peripheral_clock.freq();
    let uart = init_uart(
        pins.gpio0,
        pins.gpio1,
        pac.UART0,
        &mut pac.RESETS,
        frequency,
    );

    let delay = Delay::new(core.SYST, frequency.to_Hz().clone());

    show_version(&uart);

    let (pio, sm0, sm1, sm2, _) = //-
        pac.PIO0.split(&mut pac.RESETS);

    let mut cc2510 = Cc2510::new(
        pio,
        delay,
        2,
        1,
        50,
        50,
        sm0,
        sm1,
        sm2,
        pins.gpio16,
        pins.gpio17,
        pins.gpio18,
        pins.gpio19,
        pins.gpio20,
        pins.gpio21,
    );

    test_communication(&uart, &mut cc2510);

    // cc2510.heat_map(
    //     2029..=2060,
    //     1..=26,
    //     1000,
    //     |p| {
    //         uart.write_full_blocking(b" s");
    //         print_hex_16(&uart, p.success_count);

    //         uart.write_full_blocking(b" f");
    //         print_hex_16(&uart, p.false_success_count);

    //         uart.write_full_blocking(b" x");
    //         print_hex_16(&uart, p.screwed_count);

    //         if p.duration == 26 {
    //             uart.write_full_blocking(b"\n");
    //         }
    //     }
    // );

    uart.write_full_blocking(b"Dumping with glitch\n");
    cc2510.dump(
        0x0000..=0x7fff,
        2044,
        11,
        |b|{
            print_hex(&uart, b);
            uart.write_full_blocking(b" ");
        },
    );
    uart.write_full_blocking(b"\nDone.\n");

    uart.write_full_blocking(b"Switching off the cc2510\n");
    cc2510.power_off();

    uart.write_full_blocking(b"End.\n");
    loop {}
}

fn show_version<Rx: ValidPinRx<UART0>, Tx: ValidPinTx<UART0>>(
    uart: &UartPeripheral<hal::uart::Enabled, pac::UART0, (Tx, Rx)>,
) {
    uart.write_full_blocking(b"\n\n");
    uart.write_full_blocking(b"  |  CC2510 Glictcher v");
    uart.write_full_blocking(env!("CARGO_PKG_VERSION").as_bytes());
    uart.write_full_blocking(b"\n");
    uart.write_full_blocking(b"  |\n");
    uart.write_full_blocking(b"  |  built at ");
    uart.write_full_blocking(build_time_local!("%Y-%m-%d %H:%M:%S%:z").as_bytes());
    uart.write_full_blocking(b"\n");
    uart.write_full_blocking(b"   \\____________________________________\n\n");
}

fn test_communication<
    T: ValidUartPinout<UART0>,
    U: ValidFunction<FunctionSioOutput>,
    V: ValidFunction<FunctionSioOutput>,
>(
    uart: &UartPeripheral<hal::uart::Enabled, pac::UART0, T>,
    cc2510: &mut Cc2510<U, V>,
) {
    uart.write_full_blocking(b"Testing communication:\n");
    log_binary(&uart,b"  chip id              :",  cc2510.send(&GET_CHIP_ID));
    log_binary(&uart,b"  status               :",  cc2510.send(&READ_STATUS));
    log_binary(&uart,b"  rd_config            :",  cc2510.send(&RD_CONFIG));
    log_binary(&uart,b"  wr_config 0e         :",  cc2510.send(&wr_config(0x0e)));
    log_binary(&uart,b"  rd_config            :",  cc2510.send(&RD_CONFIG));
    log_binary(&uart,b"  debug_instr `nop`    :",  cc2510.send(&debug_instr_1(NOP)));
    log_binary(&uart,b"  status               :",  cc2510.send(&READ_STATUS));
    log_binary(&uart,b"  get_pc               :",  cc2510.send(&GET_PC));
    log_binary(&uart,b"  resume               :",  cc2510.send(&RESUME));
    log_binary(&uart,b"  halt                 :",  cc2510.send(&HALT));
    log_binary(&uart,b"  get_pc               :",  cc2510.send(&GET_PC));
    log_binary(&uart,b"  rd_config            :",  cc2510.send(&RD_CONFIG));
    log_binary(&uart,b"  wr_config 0e         :",  cc2510.send(&wr_config(0b0000_1110)));
    log_binary(&uart,b"  rd_config            :",  cc2510.send(&RD_CONFIG));
    uart.write_full_blocking(b"Done\n");
}

fn dump<
    T: ValidUartPinout<UART0>,
    U: ValidFunction<FunctionSioOutput>,
    V: ValidFunction<FunctionSioOutput>,
> (
    what: &[u8],
    uart: &UartPeripheral<hal::uart::Enabled, pac::UART0, T>,
    cc2510: &mut Cc2510<U, V>,
    range: RangeInclusive<u16>,
) {
    uart.write_full_blocking(b"Dumping ");
    uart.write_full_blocking(what);
    uart.write_full_blocking(b":");

    let _ = cc2510.send(&debug_instr_3(MOV_DPTR_CONST, *range.start()));
    for address in range {
        if (address & 0b11111) == 0b0 {
            uart.write_full_blocking(b"\n ");
        }

        let byte = cc2510.send(&debug_instr_1(MOVX_A_ATDPTR));
        uart.write_full_blocking(b" ");
        print_hex(&uart, byte as u8);
        let _ = cc2510.send(&debug_instr_1(INC_DPTR));
    }

    uart.write_full_blocking(b"\n");
    uart.write_full_blocking(b"Done\n");

}

fn init_uart(
    rx: Pin<Gpio0, FunctionNull, PullDown>,
    tx: Pin<Gpio1, FunctionNull, PullDown>,
    uart: UART0,
    resets: &mut pac::RESETS,
    frequency: Rate<u32, 1, 1>,
) -> UartPeripheral<
    hal::uart::Enabled,
    pac::UART0,
    (
        Pin<hal::gpio::bank0::Gpio0, hal::gpio::FunctionUart, hal::gpio::PullDown>,
        Pin<hal::gpio::bank0::Gpio1, hal::gpio::FunctionUart, hal::gpio::PullDown>,
    )
> {
    let rx = rx.into_function::<FunctionUart>();
    let tx = tx.into_function::<FunctionUart>();
    hal::uart::UartPeripheral::new(uart, (rx, tx), resets)
        .enable(
            UartConfig::new(115_200.Hz(), DataBits::Eight, None, StopBits::One),
            frequency,
        )
        .unwrap()
}

fn log_binary<T: ValidUartPinout<UART0>>(
    uart: &UartPeripheral<hal::uart::Enabled, pac::UART0, T>,
    label: &[u8],
    mut n: u32,
) {
    uart.write_full_blocking(label);
    uart.write_full_blocking(b" 0b");
    for i in 0..32 {
        if (i>0) && (i % 8) == 0 {
            uart.write_full_blocking(b"_");
        }        n = n.rotate_left(1);
        uart.write_full_blocking(if (n & 1) == 1 { b"1" } else { b"0" })
    }
    uart.write_full_blocking(b"\n");
}

fn print_hex<T: ValidUartPinout<UART0>>(
    uart: &UartPeripheral<hal::uart::Enabled, pac::UART0, T>,
    n: u8,
) {
    const REPRESENTATIONS: [u8; 16] = [
        b'0', b'1', b'2', b'3', b'4', b'5', b'6', b'7', b'8', b'9', b'A', b'B', b'C', b'D', b'E',
        b'F',
    ];

    let index = (n >> 4) as usize;
    uart.write_full_blocking(&REPRESENTATIONS[index..=index]);

    let index = (n & 0b1111) as usize;
    uart.write_full_blocking(&REPRESENTATIONS[index..=index]);
}

fn print_hex_16<T: ValidUartPinout<UART0>>(
    uart: &UartPeripheral<hal::uart::Enabled, pac::UART0, T>,
    n: u16,
) {
    print_hex(uart, (n >> 8) as u8);
    print_hex(uart, n as u8);
}
