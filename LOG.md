# Starting point

I did not take enough notes during the first few tens of hour of this project.
Since this is mainly me re-reading the Rust documentation and learning to use my oscilloscope, I will only document from my current achievement which is :
- I have some code that can communicate with the cc2510 on the debug interface (which is confirmed using tha `CHIP_ID` commznd that return the expected value for a cc2510)
- I can send a pusle to a transistor which draw currents on the 1.8V rail of the cc2510 (see Jasper Devreker documentation for details, though for now I use whatever BJT transistor I hade instead of a 'fast MOSFET')
- The code for the communication and glitching has been ported to rp2040 PIO peripheral using the PIO's assembly language, instead of my origial first version in Rust.
This was needed because it then become really easy to try different timing and duration for the glitch (because PIO has 1 cycle execution time for all instructions and has four execution unit, and no cache or bus matrix collision issues)
- I (only) **once** managed to get 947 executions of the command in…
1024 or 16384 attemps, I am not even sure which version of the code was running.
I did not manage to reproduce.
All I know is that the duration fo the glitch signal was 13 rp2040 cycles (at 120MHz).
This is consistent with Jasper Devreker's result which settled to 8 cycles* duration and uses a MOSFET which is probably a bit faster than my BJT transistor.
Also, he managed to completely remove the filtering capacitor on the 1.8V line.
I could do this on my first cc2510 (which I accidently destroyed), but the second one can not answer to any command unless I add at least a small capacitor (I made a circa 4nF one using aluminium foil and am still waiting for the ones I ordered)

(*: not really since, IIRC, he overclocks the rp2040 running it at 240MHz instead of 120)


## Already learnt things

### About the debug protocol
The first (probably abvious for most of you) thing I hade to understand, was what was meant by this sentend in chapter 11.2 of the datasheet:
>The debug interface uses an SPI-like two-wire interface consisting of the P2_1 (Debug Data) and P2_2 (Debug Clock) pins.
Data is driven on the bi-directional Debug Data pin at the positive edge of Debug Clock and data is sampled on the negative edge of this clock.

this means that :
- your debug interface (here, my rp2040) will drive the clock signal
- starting with the clock down (0V), it must set it to high (3.3V) and set the value to `0` or `1` (timing is not really important here) (note: you are not in a hurry to set data, what `Debug data setup` in table 21 chapter 13.6 means is that data need to be settled 5ns before clock goes low and `Debug data hold ` says it only need to be kept low for 5ns after clock low)
- once the data signal is stabilised (and the minimum timing from the cc2510 specification ellapsed), clock is set to `0` again, this will trigger the reading of the data bit by the cc2510
- once all bits are sent, we can switch the data pin from push-pull to input and send clocks signal to get the answer
- the cc2510 will try to set the data bit as soon as it sees the clock going from `0` to `1`, but `Clock to data delay` in table 21 tells it can take as long a 10ns


### About the 1.8V regulator

The 1.8V inside the cc2510 outputs a 442kHz sawtooth voltage that should be regulated with an external capacitor.
This is the capacitor I replaced with a smaller one in order to be able to faster pull the rail to 0V using to try glitching the core.

![sawtooth output of the cc2510 internal 1.8V regulator](images/regulator_sawtooth_output.png)

This is problematic when trying to glitch, because depending on the phase at which you will glitch, you will start from different value and will the recovery at different speeds, not getting repeatable results.

I imagine several solutions to this issue:
- use an opamp to detect the phase of the regulator and synch my communication and glitch on it (this iwll require at least 1MHz bandwidth for the opamp, I may not easily and cheaply find this on aliexpress)
- cut the 3.3V powersupply and start each try from a known timing after the psu is restored hoping to stay precisely in phase
- force a 1.85V potential on the 1.8V when I am not glitching it, hoping that will reset the regulator's phase
- try to glitch the cc2510 by sending a too short signal on the clock input instead of trying a too low voltage on the 1.8V rail


### About using the oscilloscope

- even if the ground of the probes are already internally connected and the aligator clips should never be connected to different potentials, you have to connect all of them to ground when measuring 60MHz signals or you will have very distorted readings
- to easily synch the capture of the oscilloscope, it is usefull to send a pulse on a dedicated pin of the microcontroler at, e.g., the beginning of a loop you want to monitor


## About the glitching timing

One could expect the test for the autorisation to execute the gdb command to be executed either once the complete commande is sent, or after the first byte of the command has been sent (the following bytes are only parameters to the command and the decision for this command do not depend on the parameters).
Of course it could occure at other timings (either because the mcu does other things before checking the authorisation, or because only a few bits ofthe command is already enough to known which authorisation will be needed), but these are the most probable ones.

The documentation by Jasper Devreker already tells that the end of first byte is the correct timing, but when I read his post I did not understand nor remember all I read.
I then rediscovered this information during the one time I managed to glitch the mcu.


## Where to go form here?

I have some code that runs on an rp2040, can communicate on the debug interface, and can send a signal to a transistor which glitches the 1.8V rail at an abitrary timing and for an arbitrary duration.

I already have a loop that tries all couples of delays and duration, rather good estimation of what the delay and duration could be, and a setup tha allows me to monitor what is happening on an oscilloscope (for local observation) and some output data on an UART (for higher level statistics).
But thing continue to fail, probably due to the need to combine many variables:
- phase shift between the 1.8V regulator and the start of the communication / glitch sequence
- delay of the glitch
- duration of the glitch
- value of the capacitor

in addition to which some combination of these parameters might reset or crash the mcu, falsening the results of the following tests.

So, instead of continuing random test, I will try to take each parameter one by one and see if that helps.


### Entering debug mode timing

The datasheet says that sending two clock pulses during with a reset low signal will enter debug mode.
It does not give precise timing information about this.
I currently use very conservativ value but if I want to be able to conduct lot of tests and extract data fast I need to use thighter timings.

Table 19 in chapter 6.11 of the datasheet says that `RESET_N low width` (*shortest pulse that is guaranteed to be recognized as a reset pin request*) is 250ns.
I don't know if sending clock signals during this period works or if I need to wait these 250ns (30 rp2040 cycles) before sending them.
I could not either find a specification for the duration of the clock pulses.
I guess they are the same as the normal debug clock timings (7.5 rp2040 cycles for one half period) but maybe it is the mcu clock (20.8ns, 2.5 rp2040 cycles).


### Debug protocol timings

The PIO can send one bit (a `0` followed by a `1` on the clock pin, with data on the data pin while clock goes `0`) every two rp2040 clock (120MHz clock, so 60MHz rate).
But the cc2510 don't answer to even a simple `CHIP_ID` command at this rate.

The speed needs to be divided by at least 5.5 to get almost always correct answer.
I decide to use a 6 diviser.
(non integer divisor is weird anyway, since e.g. 5.5 will alternate between 5 and 6, not really be 5.5 on the rp2040 PIO)

At this speed, I could determine how much time I have to wait after sending the command to send clock signal to get the results, and that is 0.
At least for `CHIP_ID`, peraphs executing an instruction with `DEBUG_INSTR` requires a bit mode time ?

The datasheet of the cc2510, in table 21 chapter 6.13 that the minimum `Debug clock period` is 125ns, (8MHz clock).
With the 120MHz divided by 6 and two PIO clocks per period, my test says it works at 10MHz (but I have some errors at 10.9MHz with 5.5 divider).
This is consistent.
Does this mean that all commands are guarenteed to run in less than 125ns (three core cycles.
IIRC the 8051 core executes all instructions in 1 cycle on this mcu) ?


### Needed time after reset - finally leading to other questions

After a reset, the mcu will need some time before it can start discussing on the debug interface.
If I move the reset handling to the PIO code, it is easy to test how long is required.

Table 19 in chapter 6.11 of the datasheet says that `RESET_N low width` (*shortest pulse that is guaranteed to be recognized as a reset pin request*) is 250ns.
But it does not tell how long after reset the mcu will be available.
Trying to search for this information, I found two interesting things.

First in chapter 11.4.2:

>The READ_STATUS command is used e.g. for polling the status of flash chip erase after a CHIP_ERASE command or oscillator stable status required for debug commands HALT, RESUME, DEBUG_INSTR, STEP_REPLACE, and STEP_INSTR.

So there is no specification for the delay, but maybe I should poll the status for this *stable status*.

Looking for the format of the answer in table 47 (14.11.3) gives the answer (`OSCILLATOR_STABLE` bit).
But looking for this answer I read the table 47 (*Debug Command*), and came across this information about the `DEBUG_INSTR` I use:
> To run this command, the CPU must have been halted.

I am not doing this.
And I looked at Jasper Devreker and don't think he does either.
This might not be a probleme: there is a possibility that the code or hardware implementation that handles the instruction tests both the halted and debug locked conditions, then `or` them and tests the result of this `or`, so we have only one test to glitch.
But there is also a possibility that these are two different tests that would each stop processus the command and that both must be glitched one after the other.

Since the condition can be verified, it might be better to first try `HALT`ing the mcu (with a glitch), check wether it has happen, and then only if it did try to glitch again to `DEBUG_INSTR`.
Or it might not (needing two glitches in a row when one would be enough)

Anyway, I now have a simpler small step that would teach me something: by trying to glitch on `HALT` instead of `DEBUG_INSTR`, I avoid the previous question.
(I also avoid having to make sure that I don't need to wait between trying executing an instruction and sending clock signals to get the answer) But this rised yet another question: When you reset the mcu, is it halted? If so, can you keep reset low and execute instruction? I *think* I saw some behaviour that could be explained by the fact that executing the instruction ends the reset/halt state, but I'm not sure and need to if I want to be able to run test in contained conditions.


### 1.8V regulator phase link to reset or power on

I'll come back to this question later, I already have plenty of things to understand before question is relevant (but it will be).


# Notes

## Redoing everything from scratch with baby steps

Well, not completely from scratch: the Rust code is fine, it can compile the PIO programs, execute them, communicate with them and show things on the UART output.
(I still use a very hacky function that writes numbers in binary format on the UART, I still have to decide wether I use a heapless string formatter or if I take time to have compile an rp2040 compatible swd probe firmare and learn to use `defmt` - which is definitively on my list but maybe not for now)

But I need to change the command I am sending, and also to move handling of the reset (and later 3V power) to the PIO for better.
The PIO opcodes have five bit that are shared between delay and side sets.
So if you want to be able to have long delays (without needing to divide the clock rate), you can not use many pins in the group of pins you are side setting.
I first design a complex solution with three state machines that needed to synchronize and which required to desolder and swap two wires on my target board, then I understood that I since only one state machine needed to change a pin direction while running (for the `data` line), I could use the three different groups of GPIO mappings on the other.


## New Starting point

I now have some new code that allows giving precise timing to the PIO for :
- reset low (start) stabililsation delay
- reset high (end) stabilisation delay
- delay of glitch (after synchronisation of the states machines)
- duration of the glitch (starting at one cycle)

Power off delay and power on delay are handled with by the core using a timer, they do not need much precision.


## 2024-02-13 - +3V capacitor

The delay for power on and power off are are realy high, about 2x5ms is required.
I removed the biggest (in size at least) capacitor between GND and Vcc, and this seems to allow lowering the delay to 2x1ms.
The capacitors seems to be about 50µf.

Two interesting things happened:
- first communication with the cc2510 no longer work. I may need to adjust timings
- sometime the output on the 1.8V lines start to show a sawtooth shape, meaning the regulator struggle in doing its job, but this is sometimes stable on the oscilloscope, meaning it has a fixes phase in my main loop.
I hoped to get this behaviour to be able to syn the glitch with a know phase of the regulator cycle to get more constitent results.

Anyway for now, communication is lost and the power on/off delay is still rather long.

Test done: rising the delay after reset from 500 to 5000 cycle solves the problem.


## 2024-04-03 - Using an analog switch

I made several tests but could not have any success in glitching.
I started to think the problem might be because I would need to hit the glitch at a proper timing with respect to the sawtooth output of the 1.8V regulator, and started to imagine a solution with two MOSFET :
- one allowing to pull Vcore to Gnd
- one allowing to push 1.85V to Vcore, just a bit above what the internal regulator tries to establish, so it will just shut down and hence be in a predictible state when we pull to Gnd and the capacitor could be cut from Vcore by the second mosfet so it won't get in the way of the first one trying to pull to Gnd.
But I don't really know how to do that, there might by some problems (e.g. switching time making both mosfets passing at the same time, shorting the 1.85V psu to Gnd).
So I took a break, watched a few more videos about glitching and… found [this one](https://youtu.be/CX71p_qcCxY?t=742) where they did exactly what I wanted but used a dedicated IC called an "analog switch" that serves exactly this purpose.

Since Radiospares has free shipping if you order during the weekends (at least on their french customer site), I browsed their catalog, found the sn74lvc1g3157 that hade a 0.65 pitch and did no zorked on first attempt, asked for help which did not worked (I burnt the second part two, which was not a probleme since I hade to order 10 parts anyway, but deadbug soldering was not much fun on third attemps), asked ofr help again (on [eevblog](https://www.eevblog.com/forum/beginners/sn74lvc1g3157-analog-switchs-keep-burning-any-hint)) and finally managed to make the switch work using a 0.5mm to 2.54mm pitchs board (that worked on first attemps with my 2x3 0.65mm pitched IC), and after making a few change to the code and adding an lm317 to have 1.85V… that still did not work.

This time the probleme is that a stalled `OUT` reading on the fifo continues asserting the side sets of the PIO, hence preventing you from executing an instruction (using the `SMx_INSTR` control register of the PIO) to turn the power on (it turned off after one cycle…).
This took time to understand, I isolated the problem, tryed to find zhere to submit a pull request to the rp2040 datasheet, found the issue had [already been reported](https://github.com/raspberrypi/pico-feedback/issues/185) a long time ago, and now I think I need to move the code to enter debug mode out of the PIO program so I can save some space in the program memory and set configure the side sets as optionnal so the stalled instruction do not have it and do not override anything.

This is the test setup (the electronic pricetag is not connected):

![](images/with_analog_switch.jpg)

- the black jumpers are tied to Gnd (wich is available on top and bottom)
- the red jumpers provide 3.3V (to the analog switch, top side) and 5V (to the lm317 in sot23 package, bottom side)
- green allows adjusting the output of the lm317 to 1.85 using the potentiometer.
this potentiometer is 500 ohm, so it also insures that the lm317 alzays output at least 3.5mA and then can regulate properly
- yellow (bottom only) is the 1.85V output of the regulator
- blue is the command of the switch

from the pin marked as `1` on the bottom left of the switch and going counter clockwise, we have:
- `B2` (tied to `1.85V` and linked to `A` when `S` is a logic `1`)
- Gnd
- `B1` (tied to `Gnd` and linked to `A` when `S` is a logic `0`)
- `A` (wich will later be connected to Vcore of the cc2510)
- `Vcc`
- `S` (tied to a GPIO and also tied to `Gnd` via a resistor so it doesn't float, not sure the later is necessary)


## 2024-04-03 - Rethinking running code on the state machines

All my code which handles GPIOs (for powering the board, communicating with it, and glitching Vcore) runs on the state machine, the C code on the Cortex core only streams parameters via the state machine's fifos.
Jasper Devreker's took a different approaches, where normal C code was used to handle most things and generate a waveform, this waveform being streamed to the fifo using the DMA.
Though this make it a bit less difficult to have complete control of the timings (even if you write assembly code trying to count cycles, cortex's pipeline invalidation, cache and bus mnatrix collision are not easy to understand… I tryed for another project!), it has big advantage of keeping the PIO code minimal which is a good thing when you have only 32 words per statemachine.

Here is the sequence:
- set Vcc and Vcore low
- let time for the cc2510 to completely turn off (long, no critical)
- set reset low
- set Vcc and Vcore high
- let time for the cc2510 to completely turn on (long, no critical, about 5000 cycles or 40us with the current home made small capacitor that should not be required once we use the switch)
- send two pulses on clock (at least 8 cycles per pulse, between pulses, and after)
- set reset high
- wait some time (long, no critical, about 500 cycles or 4us)
- synch the state machine
- set data pin as output
- send data and clock every 16 cycles (not critical, but becomes critical when you want to synch with glitch)
- glitch by setting Vcore low for N cycles after M cycles (critical)
- set data pin as input
- read answer (sending one clock every 16 cycles)

The only part that is time critical is sending the command and the glitch.


## 2024-04-03 - possible pin mapping

The pins that need to be precisely timed, hence controlled by the PIOs are:
- Data
- Clock
- Glitch

Data and clock can run on the s


## 2024-05-24 Ready for new tests

I rewrote most of the code see [notes](writting_the_pio_code).
The main goal was to reduce the size of the code by moving non time critical part from the PIO to the Cortex core to save code space.
Secondary code was to make a code that is easyer to understand and to learn a few things.
Some of the things I learnt are :
- Using PIO IRQs to synchronize the virtual machine among them and with the core
- Reading PIO registers with GDB to composensate the lack of pio debugger
- There seems to be a delay when scanning the pin with the pio
- Decoding SPI protocol with the Rigol DS1054z (all the "timeout" period needs to be shown on screen for the decoding to be displayed - I may later switch the oscilliscope trigger signasl to a "spi chip select" to help use CS instead of timeout to detect start)
- Trigering the oscilloscope on some bit pattern on an SPI stream.
This will allow capturing the first `read_status` answer trame telling the cc2510 is not locked.
The test triggering on high sixth bit of the answer looks like this:

![](images/spi_decoding.png)

The new code seems fine.
Currently if I try to read the status after a failed (because of debug lock) `halt` or `debug_instr` command, I get sommething unexpected on next `read_status` (`0`s IIRC).
I also have hade a weird behaviour where after some glitch attempt loop iteration, I got only `1`s as an answer, including for the `get_status` command.
But in the outer loop the answer was the expected one.
One hypothesis is that, since this code bugged and only sent only 8 bit, the answer was `0`s because of the debug lock, but when a glitch worked the cc2510 waited for some more bit with a pullup on the line and then I read only ones.

Anyway, I now have a code that should allow me testing and observing the behaviour.


## 2024-05-24 Studying debug command behaviour without glitch.

Before trying to detect a sucessfully glitched command, lets first try to detected that command are effectively blocked without a glitch.

There is no command that returns something other than zeroes when blocked, so we will have to rely on the `read_status` command to check the outcome.
The `halt` command should update the status, which can be read with a `read_status` command, in two ways:
- the third bit tells if the cc2510 is locked
- the fourth bit tells if the cc2510 is halted
**edit: this is wrong, 4th bit tells HOW it was halted, it bit 5 (6th) which says if it is halted**
After some tests, the `halt` command behaves strangely.
this code:
```rust
uart.write_full_blocking(b"-- Testing communication:\n");
log_binary(&uart, b"chip_id    ", cc2510.send(commands::GET_CHIP_ID));
log_binary(&uart, b"status     ", cc2510.send(commands::READ_STATUS));
log_binary(&uart, b"chip_id    ", cc2510.send(commands::GET_CHIP_ID));
log_binary(&uart, b"status     ", cc2510.send(commands::READ_STATUS));
log_binary(&uart, b"halt       ", cc2510.send(commands::HALT));
log_binary(&uart, b"status     ", cc2510.send(commands::READ_STATUS));
log_binary(&uart, b"status     ", cc2510.send(commands::READ_STATUS));
log_binary(&uart, b"chip_id    ", cc2510.send(commands::GET_CHIP_ID));
log_binary(&uart, b"status     ", cc2510.send(commands::READ_STATUS));
log_binary(&uart, b"halt       ", cc2510.send(commands::HALT));
uart.write_full_blocking(b"writing things to loose some time...\n");
uart.write_full_blocking(b"writing things to loose some time...\n");
uart.write_full_blocking(b"writing things to loose some time...\n");
log_binary(&uart, b"status     ", cc2510.send(commands::READ_STATUS));
log_binary(&uart, b"status     ", cc2510.send(commands::READ_STATUS));
log_binary(&uart, b"halt       ", cc2510.send(commands::HALT));
log_binary(&uart, b"chip_id    ", cc2510.send(commands::GET_CHIP_ID));
log_binary(&uart, b"status     ", cc2510.send(commands::READ_STATUS));
log_binary(&uart, b"pc         ", cc2510.send(commands::GET_PC));
log_binary(&uart, b"status     ", cc2510.send(commands::READ_STATUS));
log_binary(&uart, b"chip_id    ", cc2510.send(commands::GET_CHIP_ID));
uart.write_full_blocking(b"-- Done.\n");
```
gives:
```
-- Testing communication:
chip_id    : 0b00000000000000001000000100000100
status     : 0b00000000000000000000000010110110
chip_id    : 0b00000000000000001000000100000100
status     : 0b00000000000000000000000010110110
halt       : 0b00000000000000000000000011111111     // after an `halt`…
status     : 0b00000000000000000000000000000000     // …next command fails…
status     : 0b00000000000000000000000010110010     // …but works if sent again.
chip_id    : 0b00000000000000001000000100000100
status     : 0b00000000000000000000000010110010
halt       : 0b00000000000000000000000011111111
writing things to loose some time...
writing things to loose some time...
writing things to loose some time...
status     : 0b00000000000000000000000000000000    // waiting a bit does suffice to
status     : 0b00000000000000000000000010110010
halt       : 0b00000000000000000000000011111111
chip_id    : 0b00000000000000000000000000000000    // and that's not `read_status` that fails
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000000000000000000000    // `get_pc` is also blocked but does not make next command fail
status     : 0b00000000000000000000000010110010
chip_id    : 0b00000000000000001000000100000100
-- Done.

```

Maybe there is something wrong with the way I send the `halt` command, maybe I do not let it enough time to answer or read the wrong number of bits on the answer, but I could not find anything in the documentations.
Here is a capture of the trame:

![](images/halt.png)

Nothing is abviously wrong.

`chip_id` says the cip is a cc2510 (correct) revision 4 (plausible).

`read_status` says:
|       flag        |  value  |
|-------------------|---------|
| CHIP_ERASE_DONE   |     yes |
| PCON_IDLE         |      no |
| CPU_HALTED        |     yes |
| POWER_MODE_0      |     yes |
| HALT_STATUS       |      no |
| DEBUG_LOCKED      | changes |
| OSCILLATOR_STABLE |     yes |
| STACK_OVERFLOW    |      no |

After the `halt` command, the debug locked bit change.
But that probably not a sign that the halt command worked: after the `halt` the status reads as only `0`s.
Or, we know that only a few commands update the reading of the status (`halt`, `resume`,
`debug_instr`, `step_replace` and `step_instr`).
The `debug_locked` flag has just probably not been updated.

Lets do another test where we first unhalt the microctronler then try to halt it again to see if the `cpu_halted` flag changes, and try forcing the update of the flag.
Result is:
```
resume     : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010010010
halt       : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
exec nop   : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110110010011
status     : 0b00000000000000000000000010110010
```
It seems that other protected instructions have this same behaviour.
But anyway resuming seems to have worked: the `cpu_halted` flag turned `0` then `halt` turned it back to `1`.
This is confirmed by the fact that PC (the instruction pointer) changed…
Which is weird since reading PC should not be allowed on a debug locked chip.

The documentation says several times:
> For software code security the Debug Interface may be locked. When the Debug Lock bit, DBGLOCK, is set all debug commands except CHIP_ERASE, READ_STATUS and GET_CHIP_ID are disabled and will not function.

Another unexpected thing is that after reset, even the first batch of reading says PC is not zero, and it WAS zero on the first test.
I tryed resetting, cold rebooting…
I could not get the zeroes again.

What happen if I try to execute not a `nop` but an instruction that will change the `a` register?
```
exec move a: 0b00000000000000000000000011111100
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
pc         : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
exec move a: 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
pc         : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
```
Nothing I am able to explain.
I can try adding delay betzeen sending the command and reading the answer:
```
| CC2510 Glictcher v0.0.1
|  built at 2024-05-25 00:12:46+02:00
 -----------------------------------

-- Testing communication:
halt       : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010010010
halt       : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
chip_id    : 0b00000000000000001000000100000100
status     : 0b00000000000000000000000010110010
halt       : 0b00000000000000000000000011111100
writing things to loose some time...
writing things to loose some time...
writing things to loose some time...
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
halt       : 0b00000000000000000000000000000000
chip_id    : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
pc         : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
chip_id    : 0b00000000000000000000000000000000

resume     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
halt       : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
exec nop   : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
pc         : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000

exec move a: 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
pc         : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
exec move a: 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
pc         : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
-- Done.
```
With 35 cycles instead of about 16 previously, the result completely changes:
- with a 7 iterations loop, pc is read `not 0` every time
- with a 16 iterations loop, pc reads as `0`s after the `exec move a`
- with 16 and 24 iterations, the `nop` execution returns a non `0` and consistant value!

I will have to re-read the documentation (and original article) to see which timing should be used.
But maybe there is a possibility to glitch with only read timing?


## 2024-05-25 Adjusting timings.

I measured the timings on my oscilloscop and compared them to the requirements in the datasheet:

| parameter                   | measured   | specified  |
|-----------------------------|------------|------------|
| Debug Clock Period          |       64ns | min 38.5ns |
| Debug Data Setup    (write) |       31ns |    min 5ns |
| Debug Data Setup    (read)  |       16ns |    min 5ns |
| Debug Data Hold     (Write) |       16ns |    min 5ns |
| Debug Data Hold     (read)  |       44ns |    min 5ns |
| Clock to Data Delay (Write) |        0ns |   max 10ns |
| Clock to Data Delay (read)  |       44ns |   max 16ns |

Everything is in range.

The Clock to Data Delay is longer than expected, but the load on my clumsy breadboard is probably more than the specified 10pF.

Interestingly the original article uses a 11µs clock perdio, 290 more than required.

I still have that difference on "recovery" of the commucation.
It normally take one command before it recovers:
```
chip_id    : 0b00000000000000001000000100000100
halt       : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010010010
halt       : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
```
but if I add delay it won't recover:
```
chip_id    : 0b00000000000000001000000100000100
status     : 0b00000000000000000000000010110010
halt       : 0b00000000000000000000000011111110
writing things to loose some time...
writing things to loose some time...
writing things to loose some time...
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
halt       : 0b00000000000000000000000000000000
chip_id    : 0b00000000000000000110010000000000
status     : 0b00000000000000000000000000000000
pc         : 0b00000000000000000110010000000000
status     : 0b00000000000000000000000000000000
chip_id    : 0b00000000000000000110010000000000
```
But once, when I tried adding mode commands to see if it could later recover, it recovered on the second one:
```
chip_id    : 0b00000000000000001000000100000100
status     : 0b00000000000000000000000010110010
halt       : 0b00000000000000000000000011111111
writing things to loose some time...
writing things to loose some time...
writing things to loose some time...
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
```
This is for now not really reproducible nor understood.

Trying to check and adjust timings did not help.


## 2024-05-25 Getting repetable behaviour on base commands.

I can sucessfully `resume` and `halt`, and sometimes `get_pc`.
The debug commands and how they behave with respect to locking according to the datasheet is summarized here:

| command       | requires halt  | lockable       | updates status |
|---------------|----------------|----------------|----------------|
|CHIP_ERASE     |                |                |                |
|WR_CONFIG      |                |       x        |                |
|RD_CONFIG      |                |       x        |                |
|GET_PC         |                |       x        |                |
|READ_STATUS    |                |                |                |
|SET_HW_BRKPNT  |                |       x        |                |
|HALT           |                |       x        |       x        |
|RESUME         |       x        |       x        |       x        |
|DEBUG_INSTR    |       x        |       x        |       x        |
|STEP_INSTR     |       x        |       x        |       x        |
|STEP_REPLACE   |       x        |       x        |       x        |
|GET_CHIP_ID    |                |                |                |

Interestingly, the fact that only some commands update the `debug_locked` status flag is mentionned in the [CC2510Fx / CC2511Fx datasheet](https://www.ti.com/lit/ds/symlink/cc2510.pdf) but not in the [CC1110/ CC2430/ CC2510 Debug and Programming Interface Specification Rev. 1.2](https://www.ti.com/lit/ug/swra124/swra124.pdf).
Could it be that the implementation is not compliant to the protocol and needed some addition in the datasheet?
And could this implementation have other little details not disclosed in the datasheet ?

An hypothesis is that the commands that should be locked by the Debug Lock Bit are only locked once the status flag has been update by another command.

Trying to test this give give inconsistant results.
Even after rising the duration of the powercycling at start, sometime the first `halt` does not recover:
```-- Testing communication:
status     : 0b00000000000000000000000010110110
halt       : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
pc         : 0b00000000000000000110010000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
…
```
Sometimes it does:
```
-- Testing communication:
status     : 0b00000000000000000000000010110110
halt       : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
resume     : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010010010
halt       : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111001100
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111001100
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111001100
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
exec nop   : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111001100
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111001100
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111001100
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
-- Done.
```
When it does… what happens?
- the first status says the cpu is halted (bit 5, 6th one) by an `halt` and not a breakpoint (bit 3).
In fact it is probably halted because we have activated the debuging.
The Debug Locked bit (2) is at 1.
- `halt` gives some garbage `1`s (the documentation saythey need to be discarded)
- `read_status` first returns `0`s, then an updated status saying debug interface is not locked!
- `get_pc` returns `0`s.
I first interpreted this as an error saying the command is blocked, but I now have another possibility: my cc2510 is indeed unlocked and PC really is zero because we halted during reset.
- `resume` changes bit 5 of the status, telling us the mcu is no longer halted
- `halt` changes it back to `1`
- after this, `get_pc` gives a new value

So, my mcu is apaprently not locked, I don't understand why I have to read the status twice to get a value, and other things seems unstable.
One of the unstable things might be sending an `halt` when the cpu is already halted.

I tried running 20 times this sequence, and got 20 times the same result except for a slightly different value of PC:
```
status     : 0b00000000000000000000000010110110
resume     : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010010010
halt       : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111110011
```
By removing the uart output, letting only PC in the end, the result get constant (same value of PC is found)

So, we apparently can get way more repeatable results by:
- not halting the cc2510 when it is already halted (and it is after entering debug node)
- not outputing values to the UART until communicatino on the debug interface is done
- executing a dummy command after protected ones to let it recover

With this, it seems that:
- my cc2510 is not locked (I'll try backuping the firmware, flash a known one, lock and then glitch with known "firmware")
- `halt`, `resume`, `get_pc` and `get_status` are usable.


## 2024-05-25 Adding the `rd_config` and `wr_config` commands.

Adding the command did not work.
At least I always read `0`s after writting.
The programming documentation says the bit 2 can by `x`, probably saying that you can choose if the value is returned by the command, but the datasheet only says it must be 1.
Neither work.


## 2024-05-25 Finally some repeatable and usable results!

Since the original papier used a slower clock than mine, I put a clock divisor of 16 on the command sending state machine, but I still could not get back the value of register `A` after a `debug_instr` that modified it.
So I checked again everything yet another time.
The only think I changed was to use an [online assembler](https://8051-simulator.vercel.app/) to check the opcode for the `mov` instruction and an error made me understood that the `mov A, direct` I used did not get a direct value but a direct address.
So I assembled a `mov A, #abh`, got the proper opcode, modified my command and:
```
| CC2510 Glictcher v0.0.1
|  built at 2024-05-25 17:15:06+02:00
 -----------------------------------

-- Testing communication:
status     : 0b00000000000000000000000010110110
mov a      : 0b00000000000000000000000010010011
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
mov a      : 0b00000000000000000000000010010011
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
-- Done.
```
Setting the clock divider of the state machine back to 1 gave an unstable result:
```
-- Testing communication:
status     : 0b00000000000000000000000010110110
mov a      : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000011
status     : 0b00000000000000000000000000000000
mov a      : 0b00000000000000000000000011111110
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
-- Done.

…

-- Testing communication:
status     : 0b00000000000000000000000010110110
mov a      : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000001111
status     : 0b00000000000000000000000000000000
mov a      : 0b00000000000000000000000011111110
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
-- Done.
```

Now that the we can execute 8051 instruction, we can chekc the hypothesis we hade yesterday:
```
-- Testing communication:
status     : 0b00000000000000000000000010110110
config r   : 0b00000000000000000000000000000000
config w   : 0b00000000000000000000000000000000
config r   : 0b00000000000000000000000000000000
config w   : 0b00000000000000000000000000000000
exec mov A : 0b00000000000000000000000010010011
status     : 0b00000000000000000000000010110010
config r   : 0b00000000000000000000000000000000
config w   : 0b00000000000000000000000010110010
config r   : 0b00000000000000000000000000001110
pc         : 0b00000000000000000000000000000000
-- Done.
```
- The Debug Lock Bit (2) in the status byte reads as locked.
- In this situation, `RD_CONFIG` and `WR_CONFIG`, which are blocked by the protection and do not update the bit in the status return all `0`s on first iteration.
- The we send a `DEBUG_INSTR` to execute a `mov A`: the valu put in register A is returned and next reading of the status shows that the bit is Debug Lock Bit as been updated from its default value of `1` to `0`.
- After that, reading `RD_CONFIG` still returns `0`s but this time probably because it IS the value of the configuration byte.
- We then `WR_CONFIG` to change the configuration, and this time the answer is the Status.
The progranning guide says it return things that must be discarded, so I don't know if it really returns the satus or whatever happens to be in a buffer that has previously been used to send the status.
- Next `RD_CONFIG` confirms we wrote the configuration by returning it.
- Finally, since command seems to work and locking is understood, we `GET_PC` which returns `0`s and that confirms it is the current value of PC and not some protocol error.


## 2024-05-25 Did I erase the flash?

Now that communication seems stable and we know the chip is not locked, I tried dumping the flash: once it is dumped I can erase the flash, put my own data, Debug Lock the chip and try to glitch with data I can easily validate.

Dumping the flash should be easy now I can execute an instruction.
The programming guide even give the needed instruction and the opcode for this.
I ran it, tried to slow the execution to let more cycle for the instruction to run… but all I got are `0`s.

This is when a new hypothesis came: What if the cc2510 WAS Debug Locked during y first tests, but then I erased the full flash at some time, unlocking it in the process.

The `GET_PC` instructions encodes as `0b00101000`, and `CHEAP_ERASE` as `0b00010x00` (where is is apparently "return input byte to host", whatever that means).
It is perfectly possible that my tests to find the proper speed gave a 1 bit shift and that the `GET_PC` got interpreted ad a `CHEAP_ERASE`.
Of course there is some protection agains this kind of problem, which is:

> If any other command, except READ_STATUS, is issued, then the use of CHIP_ERASE is disabled.

But… I did run some tests where `GET_PC` was the second commander after a `READ_STATUS`.

Wait… let me check something…
Ok, it might have been the case that I wrote the instruction codes in binary and made an alignement error, but it is not they are all in hexa.
And the command for `CHIP_ERASE` is… `0x14`… I'm pretty sure I made a mistake converting something at some point and used this code!
Unfortunately the 1000+ lignes of log I have are not precise enough to have kept trace of the error.
`git-log` too did not help find evidence, but I think this is what happened.

Ok, so I have probably erased a chip.
I can't extract the firmare from it but I have three of them.
I thought I burnt a previous one because it no longer answered commands, but I can try it again now that I have stable schematic, code and timings.
I still have the third one thought the firmware might be a bit different since it uses a different screen.

So, lesson learnt: In communication initialisation code, just after reset I will execute a `nop` instruction that will update the Debug Lock Bit and prevent any erase of the flash.

Of course there is still a possibility that my code is buggy and this is why I read `0`s, but the previous lesson still stands true.


## 2024-05-27 Fixing the memory reading commands.

Yesterday I hacked some commands to dump the flash that *confirmed* the flash was erased and dumped as only `0`s.
Before I settled my mind on the fact I erased the whole chip, I wanted to observe the behaviour on the oscilloscope and check everything confirms what I thought and this was not another communication error.

Looking on the oscilloscope, it became abvious that the commands were only 8 bit longs: Instead of sending the command to execute the opcode followed by the opcode, I just sent the opcodem using the opcode length instead of the command length.

I fixed that and now I dump only `ff`s.
That makes more sense since the datasheet says:
> The CHIP_ERASE command will set all bits in flash memory to 1.

Here is the oscilloscope trace:

![](images/reading_ff.png)

The `DPTR` register is first set to `0x0000`, then during a loop we read what it points to and increment it.
On the trace, we see a (hexadecimal) `55` (debug interface commande `debug_instr` for an opcode length of one byte), then the opcode `a3` (`inc dptr` in 8051). After that we set the pin as input and the cc2510 drives it low until we start sending clock signal. It drives the data line high for height clock periods than releases the line and we see the data line slowly going down as nobody drives it anymore. We then send another `debug_instr` for the `movx a, @dptr` opcode (`55 e0`) but this time quickly after the last clock signal ends, even before we release the line, the cc2510 start pulling it up. I don't know why. IT then drives it low until we send the clock signals, we get `ff` again and it the cc2510 releases the line after height clock period as previsouly.

I tried extending the delay between seding the `movx a, @dptr` and sending clock for the answer to up to 130ns.
As I think the cc2510 runs at 26MHz this would be


## 2024-05-29 Last verifications before writting to the flash

It is very plausible that I erased the flash by mistake.
So I could flash some data of mine, check reading it is working, protect the flash and try to glitch.
But that would destroy the firmware I try to read if it is still there.

There are two verifications I want to try:
- Use the same code to read data that I know are not `ff`s
I can read some configure register, which will require different timings but same instructions.
- Writting then reading to RAM (also different timings)
- Try reading the two other board, one of than at least should not be erased and show different resutls.

### Reading and writting SRAM and SFRs

- Mistakingling sending an invalid opcode returns all `ff`s
- Handwritting the opcode thinking that values are entered in big-endian makes the code read random `0x00`s and `0x01`s
- With some I think is the proper code I get only `0x00`s, which should not be the case since some SFRs have not 0 reset value.
I tried using 2, 8, 16 and 31 iterations of a 8 cycle delay loop with a 32:0 clock divisor, all with same result.

I tried reading aroudn the end of the flash space to see if I see somethin different in flash space and in the unimplemented following zone.
This returned only `ff`s but changing some value I came accross one that returned some real data:

```rust
    let _ = cc2510.send(commands::Command(32, 8, 0x5790f01f));
```
Playing with the values showed that endinf the opcode with `f11f` instead od `f01f` resulted in a 256 bytes offset in the output.
So either the value is not little-endian.
If this is the case, I should be reading the SRAM through the slow access area (that can hold code) thrue xcode memory space.
To check this hypothesis, lets dump the address space around the start of the memory area:

We hope to start at `0xfe00`:
```rust
    let _ = cc2510.send(commands::Command(32, 8, 0x5790ef00));
```
then dump 512 values:
```rust
   for i in 0x0000..=0x1ff {
        // MOVX A, @DPTR
        let byte = cc2510.send(commands::Command(16, 8, 0x55e00000));
        // INC DPTR
        let _ = cc2510.send(commands::Command(16, 8, 0x55a30000));
   }
```
(code to communicate the resulta have been removed)

And we get:
```
-- Dumping.

FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
20 08 7B CD B5 10 DB CE 8F FB 94 15 8B 57 29 A0 B0 92 EE F7 A1 41 D2 BD 9A 8F 82 4C FA FD E4 22
02 50 0D DB 52 20 61 EB FB FF 09 4C F5 2E 12 58 21 40 12 9B 31 22 75 B9 FF FF 84 91 BF 8C 0C A4
AB EB CD 88 F8 DE 4C A0 00 E0 E1 92 82 41 FF D9 30 6E 30 83 0F DF 08 C0 02 01 AE 7E 3C 00 F2 DD
FF EC 5D 21 B7 7D 20 04 80 D4 8C 98 2B 80 F1 AF FF CF 15 98 6F 87 55 0A 81 04 65 0E B0 B1 FB F7
41 00 6A C5 60 81 F6 1E B8 DF 88 88 1F FE 7C C4 0B 02 B0 0A 04 01 E6 7D A3 7E 08 A0 CF FB A8 25
00 E5 A6 E4 41 29 ED BC FF DE 18 03 7D FE 6A 54 0C A2 ED A4 24 8E F9 EE BD BE 25 0A FF CE 93 C9
6D FF 48 00 FE 77 35 0A 05 02 EF FD 50 40 A2 9B 9B FF 88 C2 FF FB 94 48 02 00 36 1B 40 00 EF 57
BF 5F AF 91 DF F5 80 89 90 1B DF 72 10 01 DF FF ED F7 29 26 7F DF 13 01 6A 01 7F C2 08 22 7F 9F
```
The result is stable.
The datasheet says SRAM contains random values at start.
The result seems good.
It does not explain why we red only `00`s on sfr.

Lets try to write to memory to be sure:
```rust
        // MOVX A, <lower 8 bits of i>
        let _ = cc2510.send(commands::Command(24,  8, 0x56740000 + ((i & 0b11111111) << 8)));
        // MOVX @DPTR,A
        let _ = cc2510.send(commands::Command(16,  8, 0x55f00000));
```
(we add this lines in the loop before the dumping code).

We get:
```
-- Dumping.

FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F 10 11 12 13 14 15 16 17 18 19 1A 1B 1C 1D 1E 1F
20 21 22 23 24 25 26 27 28 29 2A 2B 2C 2D 2E 2F 30 31 32 33 34 35 36 37 38 39 3A 3B 3C 3D 3E 3F
40 41 42 43 44 45 46 47 48 49 4A 4B 4C 4D 4E 4F 50 51 52 53 54 55 56 57 58 59 5A 5B 5C 5D 5E 5F
60 61 62 63 64 65 66 67 68 69 6A 6B 6C 6D 6E 6F 70 71 72 73 74 75 76 77 78 79 7A 7B 7C 7D 7E 7F
80 81 82 83 84 85 86 87 88 89 8A 8B 8C 8D 8E 8F 90 91 92 93 94 95 96 97 98 99 9A 9B 9C 9D 9E 9F
A0 A1 A2 A3 A4 A5 A6 A7 A8 A9 AA AB AC AD AE AF B0 B1 B2 B3 B4 B5 B6 B7 B8 B9 BA BB BC BD BE BF
C0 C1 C2 C3 C4 C5 C6 C7 C8 C9 CA CB CC CD CE CF D0 D1 D2 D3 D4 D5 D6 D7 D8 D9 DA DB DC DD DE DF
E0 E1 E2 E3 E4 E5 E6 E7 E8 E9 EA EB EC ED EE EF F0 F1 F2 F3 F4 F5 F6 F7 F8 F9 FA FB FC FD FE FF
-- Done.
```
At least some small victory!

We have proofs we can read and write SRAM, operations that require 3-10 and 4-11 cycles.
I guess we are in the best case since our instructino dont need to be fetched from the flash nor SRAM and no bus collision can occure.

I apparently can read the SRAM via three memory spaces:
- XCODE, using `movec`
- DATA, using `mov` and 8 bit adressing (fast access)
- XDATA, using `movx` and 16 bit adressing (slow access)

I did not find reasons why the timings that work to dump the SRAM would not work to dump the flash.

There are still two things I do not understand:
- Why SFRs that should no all be initialised a `0` all read as `0`
- Why the online 8051 assembler/simulator assembled `mov DPTR, #1234` into `0x90 0x34 0x12` when I need to use big-endianess.

I ran the code to read the SFRs again, forgot to remove the code that wrote into the SRAM, so it wrote into the SFRs, loosing any value that should be retained between power offs, and after removing it the SFRs finally dumped though I don't know what changed that:
```
D3 91 FF 04 45 00 00 0F 00 5E C4 EC 8C 22 02 22 F8 47 07 30 04 76 6C 03 40 91 56 10 A9 0A 20 0D
59 3F 3F 88 11 0B 00 00 00 00 00 00 00 00 C6 00 00 00 00 00 00 00 81 04 00 7F 80 01 00 94 00 00
00 78 00 00 00 00 93 E2 04 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
00 00 00 00 00 00 00 00 00 50 FC 1F 00 00 00 00 00 00 00 00 00 00 00 00 00 17 00 00 00 00 00 00
00 00 70 6B 87 61 06 00 00 00 00 11 00 00 00 00 00 00 00 08 33 10 00 00 00 00 00 00 FF FF 2C 90
00 00 00 00 02 00 C9 01 00 00 00 08 40 00 40 00 00 00 00 00 00 00 00 00 40 00 00 00 00 00 00 00
00 00 00 00 00 40 40 40 00 00 00 08 40 00 40 00 00 00 00 00 00 00 00 00 00 00 00 02 00 00 00 00
FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
…
```
I could also confirm reading to SFRs works.

The flash still reads as all `ff`s so now I'm convinced I erased it.


### Testing the first board

I tried plugin the first board, the one that did not answer, and it still does not with the new schematic and code.
I consider it dead.


## 2024-06-02 Writting to the flash

After spending lot of time debugging a (useless) new functinnality to switch off the main and core psu when tests are done running (the side set on the `out` of the glitching state machine permanently reasserted VCore), the code was rather easy to write, mainly reusing existing functions and following diagram from the doc.
Unfortunately it did not work.
There is a 40µs timeout to write to the flash and maybe communication is too slow, but if this is the reason I will have to find how to have a faster communication that work (maybe the delay I use between command and answer needs to be adjusted for each commad)

Next day note: just have to write the code in RAM and execute it from here. Just as they do in the documentatin…
First thing is to write code to copy a block of data to the RAM of the cc2510.


## 2024-06-09 Needed a break from trying to write that flash

So I uploaded the code to ram, executed it and it kind of worked.
First iteration a probleme in the cc2510 machine code I seemd to have made a infinite loop and wrote `0x1234` everywhere in the flash and not just the first 16 bits.
I corrected it but then read random values only in the flash whene dunping it.
I tried to add a loop in the rust (rp2040) side to wait for the write to be done before I dumped but it did not change anything.
So I gave up and waited for about a week before I felt like working on this again.

Yesterday I ran the code again, still random results, so I went back to previous version of the code thinking I might have made a mistake in previously working code and… dunped this:
```
Dumping flash at 0x0000...
  10 10 12 34 12 34 10 10 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
Done
```
Apprently some of my attempts at writting only once after the infinit `0x1234` worked.

So I went back to the last verison of the code to try to see what was going wrong, but this time found this:
```
Dumping flash at 0x0000...
  10 10 12 34 12 34 10 10 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
  12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34 12 34
Done


Executing code...
Done
Dumping flash at 0x0000...
  1B 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 66
  66 66 66 66 66 66 66 66 66 66 66 66 66 66 66 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34
  34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34
  34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34
  34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34
  34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34
  34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34
  34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34 34
Done
```

Apparently the reading is only random after I write, not before.
I probably need to work on understanding the timing and synchronization better…


## 2024-06-09 Synchronize code with flash controler.

There are two things I need to try:
- wait fot he cc2510 code to be done before continuing execution of the rp2040 code
- check the satus of the flash controler of the cc25010 in a loop until it is ready before dumping the flash

I tried the first one already by checking the value of the PC, but I will probably do something better by ending the cc2510 code with a `trap` instruction and wait till `DEBUG_STATUS:CPU_HALTED` is `1`.

Ok, apprently the problem was needing to wait for `FCTL:BUSY` to go low before starting to write.
It works, I can write where I want in the page.
Now I need to write to the Flash Information Page to protect the cc2510 again.

Once it has been done, after needed to invert some writings to SFR and waiting for statuses. I finally could write at difference places in page 0:
```
Executing code.          Started.
Waiting for TRAP instruction to be reached. Done.

Dumping flash at 0x0000...
  FA B0 13 37 FF FF FF FF FF FF FF FF FF FF FF FF FA B8 13 37 FF FF FF FF FF FF FF FF FF FF FF FF
  FA B4 13 37 FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
Done

Switching off the cc2510. Done.

End.
```


## 2024-06-09 Locking the cc2510.

I first need to collect the information about locking which are a bit scattered in the datasheet:

> The lock protect bits are written as a normal flash write to FWDATA but the Debug Interface needs to select the Flash Information Page first instead of the Flash Main Page.
> The Information Page is selected through the Debug Configuration which is written through the Debug Interface only.

> Table 46: Debug Configuration. Bit 0 `SEL_FLASH_INFO_PAGE` Select Flash Information Page in order to write flash lock bits (1 KB lowest part of flash).
> 0 Select flash Main Page
> 1 Select Flash Information Page

> Debug Lock bit
> This information is contained in the Flash Information Page <...> at location 0x000

The table 44: `Flash Lock Protection Bits Definition` tells which bits needs to be set.

So we need to:
- select the Debug Information Page using the `WR_CONFIG` command,
- write `0x00` at flash address `0x000`.

After a quick and dirty implementation, and adding the execution of a `nop` to refresh the status, we get:
```
Status :0b00000000000000000000000010110110
```
(bit 2 is set, telling us the mcu is locked !)

After a first refactoring, we can do a full chip erase / flash / lock cycle:

```
  |  CC2510 Glictcher v0.0.1
  |
  |  built at 2024-06-11 11:35:40+02:00
   \____________________________________

Testing communication:
  chip id              : 0b00000000_00000000_10000001_00000100
  status               : 0b00000000_00000000_00000000_10110110
  rd_config            : 0b00000000_00000000_00000000_00000000
  wr_config 0e         : 0b00000000_00000000_00000000_00000000
  rd_config            : 0b00000000_00000000_00000000_00000000
  debug_instr `nop`    : 0b00000000_00000000_00000000_00000000
  status               : 0b00000000_00000000_00000000_10110110
  get_pc               : 0b00000000_00000000_00000000_00000000
  resume               : 0b00000000_00000000_00000000_00000000
  halt                 : 0b00000000_00000000_00000000_00000000
  get_pc               : 0b00000000_00000000_00000000_00000000
  rd_config            : 0b00000000_00000000_00000000_00000000
  wr_config 0e         : 0b00000000_00000000_00000000_00000000
  rd_config            : 0b00000000_00000000_00000000_00000000
Done
Erasing chip
Status                 : 0b00000000_00000000_00000000_10110010
Dumping flash at 0x0000:
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
Done
Flashing test data
Dumping flash at 0x0000:
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FA B8 13 37 FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
Done
Locking
Status                 : 0b00000000_00000000_00000000_10111110
Dumping flash at 0x0000:
  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Done
Switching off the cc2510
End.

```

We can now try glitching the cc2510 knowing that we want to find `fa b8 13 37` at `0x000f`.


## 2024-06-09 Glitch attempt 001.

I ran a test in which, on the locked cc2510, I try to execute a `mov a, #11100100b` instruction.
Since executing an instruction returns the value of the `a` register, validating the result should be easy.

The test showed no sign of success.

Details of the test:
- for a delay of 1000 to 7000 cycles (8.33us to 58.33us after starting sending the command)
- glitch for a duration of 1 to 19 cycles
- attempt 99 times before trying next value

The delay range limites are shown by the cursors on this screenshot:

![](images/attempt_001.png)

Magenta is the 5V of for the cc2510, yellow is `CLOCK`, blue is `DATA` and cyan is the glitched 1.8V for the core.

A more detailed view shows the 19 different duration of the glitch:

![](images/attempt_001_detail.png)

We can see the result of the  19 different durations, and how the result is cleaner with the analog switch than it was whith just a mosfet trying to pull down the sawtooth output of the internal voltage regulator fo he cc2510.

Both traces show exactly what I expected, but still there is no positiv result.

If instead of trying to detect a result of `0b11100100`, I log on non `0b00000000` values, I have a lot of hits.

Hypothesis about what could go wrong:
- failed glitches put the mcu in a state where it can't communicate or act correctly. (try power cycling between attemps, unlock CPU to find for proper power cycle timing before)
- the bump on `CLOCK` during the glitch attempt perturbates. (try filtering capacitor on `CLOCK` and `DATA`)
- the timing for glitching are very dependant on the instruction and glitching with a `mov a, const` won't work

## 2024-06-09 Glitch attempt 002 - full power cycle between each attempt.

I tried switching on and off the cc2510 between each attemps, with a delai of 5500 to 6500 and duration of 1 to 19.
That was of course very slow and gave no result.

I need to do three things:
- only power cycle if the previous attemps gives a non zero value but not the good one either
- unlock the cc2510 and find the proper timing for resetm power on and power off.
- study the original article to better understand the timing that should be scanned.

### Jasper Devreker working timings

The bar graph shows an optimal delay of 33513 but no unit is provided.
I onces managed to understand the unit by looking at the timing graph which show communication and... glitch attempt on bit 8 and not last bit onf the command as I did in my code and looked at during hours on the oscilloscop...

### code modification

So I changed the timing so it spreads over 8th bit (delay from 1800 to 2100, duration from 1 to 15, 50 attempts par couple), and also added a test to power cycle the cc2510 only if previous attempt answers something else than zero, but still not the expected result.

when power cycling, I print an `r` (for "reset").
I print a trace for every different delay (my outer loop) and print status and duration when the result is `0b11100100`.

Power cycling less ofter gave a very faster attempt, and an output provided in [attempt002], which interesting part starts here:
```
delay     0b00000000_00000000_00000111_11100001
delay     0b00000000_00000000_00000111_11100010
r 0b00000000_00000000_00000111_11100010
delay     0b00000000_00000000_00000111_11100011
r 0b00000000_00000000_00000111_11100011
delay     0b00000000_00000000_00000111_11100100
r 0b00000000_00000000_00000111_11100100
delay     0b00000000_00000000_00000111_11100101
r 0b00000000_00000000_00000111_11100101
delay     0b00000000_00000000_00000111_11100110
delay     0b00000000_00000000_00000111_11100111
delay     0b00000000_00000000_00000111_11101000
r 0b00000000_00000000_00000111_11101000
r 0b00000000_00000000_00000111_11101000
r 0b00000000_00000000_00000111_11101000
r 0b00000000_00000000_00000111_11101000
delay     0b00000000_00000000_00000111_11101001
r 0b00000000_00000000_00000111_11101001
r 0b00000000_00000000_00000111_11101001
delay     0b00000000_00000000_00000111_11101010
r 0b00000000_00000000_00000111_11101010
r 0b00000000_00000000_00000111_11101010
delay     0b00000000_00000000_00000111_11101011
r 0b00000000_00000000_00000111_11101011
r 0b00000000_00000000_00000111_11101011
r 0b00000000_00000000_00000111_11101011
delay     0b00000000_00000000_00000111_11101100
delay     0b00000000_00000000_00000111_11101101
delay     0b00000000_00000000_00000111_11101110
delay     0b00000000_00000000_00000111_11101111
delay     0b00000000_00000000_00000111_11110000
delay     0b00000000_00000000_00000111_11110001
delay     0b00000000_00000000_00000111_11110010
duration  0b00000000_00000000_00000000_00000111
status    0b00000000_00000000_00000000_10110010
duration  0b00000000_00000000_00000000_00000111
status    0b00000000_00000000_00000000_10110010
duration  0b00000000_00000000_00000000_00000111
status    0b00000000_00000000_00000000_10110010
duration  0b00000000_00000000_00000000_00001000
status    0b00000000_00000000_00000000_10110010
delay     0b00000000_00000000_00000111_11110011
duration  0b00000000_00000000_00000000_00000110
status    0b00000000_00000000_00000000_10110010
duration  0b00000000_00000000_00000000_00000110
status    0b00000000_00000000_00000000_10110010
```
after delay `2017`, we start to see some wrong outputs that trigger resets.
This continue until delay `2027`, then at delay `2034`...


\o\ /o/  \o/

for a duration of `7` the returned value is the expected one and status bit 2 is `0` meaning the chip thinks it is unlocked !

Lots of other positive traces occure, until delay reaches `2054`. Starting at delay `2056` we stop hanving positiv results and start see some resets again.


## 2024-06-12 Glitch attempt 003 - some statistics.

We have hade posetiv results from delais 2017 to 2054.
Lets scan from 1900 to 2100, with delay from 0 to 64, and 256 attempts per couple, counting each king of result to draw graphs.

Bad idea: reseting in loops is too slow, let try with a narrower range:
- delay 2010 to 2060 in 1 increment
- duration 1 to 32 included in 1 increment
- 256 attemps per (delay;duraiton) couple
- laptop and oscilloscop disconnected from main power supply (huge, non constant interface on 230V here ! )

The wrong non zero ratio is rather low, but not inexistant:

![](images/attempt003_wrong_non_zeros.png)

I will need to check if any of them returns a status that could let me think it is good when it is not.
Also it could be a good idea to have stats on these wrong values (if they are all `0xff`s, they will be easyer to detect).
Anyway, the ratio being low (max 7.5%, rounded 0.1 on the picture), making several dumps and comparing them should suffice.

The success ratio is way higher, up to 70%:

![](images/attempt_003_successes.png)

It show an interesting pattern, which tells me I could try longer durations.
The best duration is 11.
This does not match the value from the original article (8cycles, but with a twice faster clock which should indicate my best would be 4 cycles)

My result are way better than what the original article states:

> The best success rate for a single glitch we’ve gotten is about 5%, but this quickly drops after running for an extended time, likely because of heating.

This could be because:
- they run a different instruction than I do (I used the simplest possible one, which does not access the flash nor the ram)
- they do not use an analog switch and an external voltage regulator
- other things I dont know about like length of wiring, design of the rp2040 board, the board I attack...
- my protocol to measure succes rate is wrong or give results that can't be compared to their

But it is a very good thing because to successfully retrieve a byte the way they did*, two successes in a row are needed. With their 5% max success ratio, they need (1/0.05)^2=400 average attempts to get a byte.
With a 70% ratio, that would decread to (1/0,7)^2=2... about 200 times faster!

*: I think I have other ideas to test


## 2024-06-12 Glitch attempt 004 - more statistics.

I ran another test to collect more precise stats.
- based on previous results, I'll scan delais from 2029 to 2060 and duration from 1 to 64
- I'll remove the power cycling to see if thing continue to work
- I'll log, instead of the power cyle count, the number of time the status reports the chip is unlocked but the result is wrong
- for each (duration;delay) I'll run 10.000 attempts instead of 256 in previous run.

Not a single positiv result.
I thenk tried to power cycle the cheap again when I got a non zero while status says the mcu is not protected, and that showed many reset start to appear once duration goes higher than about 30.

I may let a long test run during a night, but for now I will fallback to exploring the promising region between 0 and 24 duration, with a high number of iteration and trying checking for rate of positive status with wrong answers.

The running test is finally:
- delay from 2029 to 2060 and duration from 1 to 64
- power cycling after a non zero answer and the cpu reporting as locked
- tracking number of success, and number of fails that reported as not locked
- 10.000 attempts instead of each pair of duration and delay
- After 11 delays, I hade to plug the psu again.

Here is a map of the percentage of success:

![](images/attempt004_successes.png)

Notable results:
- out of 832x10.000 tests, none reported an unlocked mcu but gave a wrong answer
- the shape seems close to the attempt003
- plugin the psu does not seem to have affected the output
- the best result is 66% for a delay of `2044` and  duration of `12`

Next time is to try dumping the protected flash.

## 2024-06-13 more stats.

I re-run the test to check that refactoring is ok.
Result where very comparable, and this time I also graphed the "screwed" attempts (when I decide I need to powercycle):

![](images/attempt004_wrong_non_zeros.png)

The number are for 10.000k attempts, not in percent like the previous one.

To summarize:

- (2044; 12) is till the best value for success rate
- at 3 powercycle for 10.000 attempts, these value wont cause much trouble
- there is apparently no reason to try scanning on longer than 23 cycles glitches, as succes gets lower and erros rise
- I did not make a graph of the false positives (status says the mcu is unlocked but the result is wrong), because it has always been zero, in several millions of attempts.
Be carefull thought that this mays only be right for the `mov a, CONST` instruction, maybe the `CONST` part is sometime returned instead of the value of the `a` register and this test does not detect it.

Here is the shape I next to the failing edge of the clock for the height bit of the command:

![](images/attempt_004_wave_form.png)

The glitch starts about 250ns after the clock goes down.

## 2024-06-13 Planning next moves.

- [x] check that we can actually dump flash and test the false successes rate using a flash with known content.
- [ ] solder wires on the last board I have and dump it
- [ ] add comments on the rust API
- [ ] make a synthetic writeup of the project

There are also things I wanted to do but may not:

- [ ] try uploading some code to the RAM that dumps the flash in SPI (the success rate i high enough so I don't need days to dump the flash... expecting the result are the same for flash reading instrucitons)
- [ ] check if instead of needed to success twice in a row to get one data, we can retry until first instruction passes, then try until second one passes
- [ ] fine tune the reset / power cycle delays

## 2024-06-13 Checking other instructions.

### GET_PC

I made a quick heatmap with only 1000 iteration, executing two commands:
- a `DEBUG_INSTR` with an `LJP 1377h` instruction (I started with `1234h` but too few `1` bits for a good test ^^) to load PC
- a `GET_PC`

The check of the result value and status is only done on the second one - my guess is that after the first one is executed correctly at least once PC keeps the same value and we can do stats on a single instruction.

The success and fake success rate confirm this, they are effectively the same as the previous test.
No fake positives is detected.

The screwed rate is about twice the previous one, which makes sense since we execute two instructions.
The numbers are so low with only 1000 iteration that they may not be really statistically representativ anyway (max: 8 screwed for 1000 iterations)

### DEBUG_INSTR `MOV A, @DPTR`

On first attempt I forgot to re-set `DPTR` after powercycling the cc2510, but then the result were fine with very still 64% success reading the flash, no fake success detected and very few powercycles needed in the high success area.

Since I always read the same address, there is still a possibility that the reading missbehaved and returned an old value.


## 2024-06-13 Soldering wires the last test board.

All the pins I nees are available on testpoints on side of the board, except the `DCOUPL` that give access to the 1.8V rail of the core.

![](images/third_board_pinout.jpg)

The first board hade none of the four planned capacitor, the second hade juste one, this one has three. I'll remove them.

The rewriting of the code was meant at mainteninig and using the code more easily (that was a success) and at freeing space in the PIO code space.
An additionnal gain is that now, no pin need to be consecutiv to each other, except that the pin which trigger the signal for the oscilloscope needs to be the one before glitch pin.
Now that I have learnt how to trigger the oscilloscop on specified SPI messages, I no longer use the trigger pin but anyway it does not bother me for wiring the board.

The ground pin is not movable though.
Instead of having a wire that would cross all the other to join the a distant GND of the the RP Pico, I first planned to take GND on the horizontal line of the breadboard like this:

![](images/wiring_option_1.png)

But:
- it would be hard to glue the short with the other to easy manipulation
- it may cause probleme to reuse on another breadboard

The nice thing is that `GND` is also routed on the bottom side of the capacitors I will meed to remove (so I can power cycle the cc2510 at proper speed), so I can go like this instead:

![](images/wiring_option_2.png)

This way wires will be easy to glue together.

![](images/last_board_wired.jpg)

(I need to add another layer of glue to make everything clean and smooth, but adding a thinner one allowed me to plug the board one hour after I put the glue, the last layer will wait during a whole night).
I use cheap brown packing tape to protect the surface before applying the glue, since cyanoacrylate glue ("super glue") does not stick to the non sticky face.


## 2024-06-13 Heatmapping on the new board.

I just hade to move a parameter in the `Cc2510::new` function signature to reflect the new wires order, and test worked.
(it worked on second attempt because I forgot to change the code that tested reading data that was known to be on the other card)

The heatmap is slightly different, with best result being 60% at (2044; 11) instead of 66% at (2044; 12):

![](images/last_board_heatmap.png)

The screwed count is a bit higher in the two blocks starting at delay `2055`, but zero nearly everywhere else.
