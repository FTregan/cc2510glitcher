# Glitchage de microcontrolleur, quand on sait pas faire.

Bonjour, moi c'est Fabien.
Dans la vraie vie pour mes clients je fais du dev, principalement des applis web, donc du front, du back, un peu de base de donnees, des choses comme ca.
Plus ca va plus je fais de l'accompagnement en techniques de code, agilite…
Autant vous dire que quand sur mon temps libre quand je programme sur des microcontroleur, que je fais un peu d'electronique et encore plus quand je m'interesse a la securite, je suis en dehors de mon domaine d'expertise, et encore plus en dehors de ma zone de confort.

C'est pour ca qu'aujourd'hui je ne vais pas parler de "glitcher des microcontroleurs", je vais vous parler de "glitcher des microcontroleurs *quand on sait pas faire*".

Ca va avoir quelques avantages pour vous, surtout si vous etes developpeur comme quoi :
- deja la presentation devrait etre relativement accessible
- vous pourrez peut etre y piocher des trucs que je me suis developpes pour arriver a faire quand des choses pour lesquelles je ne me sens absolument pas arme

Mais aussi quelques inconvenients :
- si vous avez des questions a la fin de ce talk, il y a des chances que je n'ai pas de reponse
- je vais parler de moi, de mes tests.
  Donc si vous n'etes pas fan de storytelling, il va y en avoir un peu quand meme mais ne vous inquietez pas on va quand meme pas mal parler de technique.

## Plan

- Les microcontroleurs (notions)
- Le glitchage (notions)
- Je me lance
  - Une demande
  - L'article de depart
  - La demande se precise
- Etat des lieux
  - Du bien
    - Raconte, explique
    - Code dispo
    - Montage simple
  - Du moins bien
    - Imprecisions ("Mosfet rapide", les timings pas compris sur le graphique)
    - Python
    - Laisser tourner plusieurs jours ( + architecture)
  - Vous voyez le probleme ?
    - Travail de production pas d'apprentissage
    - Seul
- Refaire, autrement ?
  - Autonome
  - En Rust
  - Mais je sais pas faire...
  - Il faut savoir faire QUOI ?

- Conclusion
  - youhou, ca marche
  - des trucs biens
    - rust
    - publier des notes
    - avoir quelqu'un a qui parler du projet (reprise apres pauses)
  - des trucs moins biens
    - les build / flashage... (archi d'origine pas deconnante)
    - pas du tout assez de notes (historique des commandes, commits, ...)
  - on pourrait continuer
    - extraction du firmware via du code pose en RAM
    - reverse le firmware (cc2510 pas tout a fait un 8051, pb avec les outils de reverse)
    - plutot que tirer sur la regulation, couper l'alim avant le regulateur ?
    - jouer avec la forme d'onde
    - s'attaquer a des microcontroleurs un peu moins faciles
