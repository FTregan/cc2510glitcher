## Les soudures

Bon en fait y'avait quand meme a apprendre !

![](soudure1.jpg)
![](soudure2.jpg)

## Le protocol de debug

- Utilisation de get_chip_id

The first (probably abvious for most of you) thing I hade to understand, was what was meant by this sentend in chapter 11.2 of the datasheet:
>The debug interface uses an SPI-like two-wire interface consisting of the P2_1 (Debug Data) and P2_2 (Debug Clock) pins.
Data is driven on the bi-directional Debug Data pin at the positive edge of Debug Clock and data is sampled on the negative edge of this clock.

this means that :
- your debug interface (here, my rp2040) will drive the clock signal
- starting with the clock down (0V), it must set it to high (3.3V) and set the value to `0` or `1` (timing is not really important here) (note: you are not in a hurry to set data, what `Debug data setup` in table 21 chapter 13.6 means is that data need to be settled 5ns before clock goes low and `Debug data hold ` says it only need to be kept low for 5ns after clock low)
- once the data signal is stabilised (and the minimum timing from the cc2510 specification ellapsed), clock is set to `0` again, this will trigger the reading of the data bit by the cc2510
- once all bits are sent, we can switch the data pin from push-pull to input and send clocks signal to get the answer
- the cc2510 will try to set the data bit as soon as it sees the clock going from `0` to `1`, but `Clock to data delay` in table 21 tells it can take as long a 10ns

## Optimisation des timings

  - testables
  - infos manquntes dans la datasheet
  - reduction du condensateur

## Utilisation du PIO

  - Timing et execution parallele
  - Une bonne mauvaise idee ! (pas assez simple)
  - Datasheet pourrie

## Regulation du 1.8V

The 1.8V inside the cc2510 outputs a 442kHz sawtooth voltage that should be regulated with an external capacitor.
This is the capacitor I replaced with a smaller one in order to be able to faster pull the rail to 0V using to try glitching the core.

![sawtooth output of the cc2510 internal 1.8V regulator](../images/regulator_sawtooth_output.png)

This is problematic when trying to glitch, because depending on the phase at which you will glitch, you will start from different value and will the recovery at different speeds, not getting repeatable results.

I imagine several solutions to this issue:
- use an opamp to detect the phase of the regulator and synch my communication and glitch on it (this iwll require at least 1MHz bandwidth for the opamp, I may not easily and cheaply find this on aliexpress)
- cut the 3.3V powersupply and start each try from a known timing after the psu is restored hoping to stay precisely in phase
- force a 1.85V potential on the 1.8V when I am not glitching it, hoping that will reset the regulator's phase
- try to glitch the cc2510 by sending a too short signal on the clock input instead of trying a too low voltage on the 1.8V rail

![](../images/with_analog_switch.jpg)

## obtenir un resultat repetable

```
-- Testing communication:
status     : 0b00000000000000000000000010110110
halt       : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
resume     : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010010010
halt       : 0b00000000000000000000000011111111
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111001100
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111001100
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111001100
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
exec nop   : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000000000000
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111001100
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111001100
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
pc         : 0b00000000000000001101110111001100
status     : 0b00000000000000000000000010110010
status     : 0b00000000000000000000000010110010
-- Done.
```

- Pas de halt pendant le halt
- Sur un halt normal, une commande de recuperation (pas vue dans la doc, premiere commande protegee qui update)
- Seulement certaines instruction remettent le bit de lock du status a jour
- La valeur lue dans le status est reellement utilise
- Valeur random PC : due au temps de l'UART
- Mon mcu n'est plus locke !

## On recommence en sachant que c'est unlocke

- Assembleur en ligne qui se trompe d'endianess
- Timeout sur l'ecriture de flash -> code en RAM
- On remet let lock et enfin :

```
  |  CC2510 Glictcher v0.0.1
  |
  |  built at 2024-06-11 11:35:40+02:00
   \____________________________________

Testing communication:
  chip id              : 0b00000000_00000000_10000001_00000100
  status               : 0b00000000_00000000_00000000_10110110
  rd_config            : 0b00000000_00000000_00000000_00000000
  wr_config 0e         : 0b00000000_00000000_00000000_00000000
  rd_config            : 0b00000000_00000000_00000000_00000000
  debug_instr `nop`    : 0b00000000_00000000_00000000_00000000
  status               : 0b00000000_00000000_00000000_10110110
  get_pc               : 0b00000000_00000000_00000000_00000000
  resume               : 0b00000000_00000000_00000000_00000000
  halt                 : 0b00000000_00000000_00000000_00000000
  get_pc               : 0b00000000_00000000_00000000_00000000
  rd_config            : 0b00000000_00000000_00000000_00000000
  wr_config 0e         : 0b00000000_00000000_00000000_00000000
  rd_config            : 0b00000000_00000000_00000000_00000000
Done
Erasing chip
Status                 : 0b00000000_00000000_00000000_10110010
Dumping flash at 0x0000:
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
Done
Flashing test data
Dumping flash at 0x0000:
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FA B8 13 37 FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
  FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF FF
Done
Locking
Status                 : 0b00000000_00000000_00000000_10111110
Dumping flash at 0x0000:
  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
Done
Switching off the cc2510
End.

```

## Alors on glitch

  ![](../images/attempt_001.png)
  ![](../images/attempt_004_wave_form.png)
  ![](../images/attempt_001_detail.png)

- detection des plantages (non zero, mauvais resultat)

```
delay     0b00000000_00000000_00000111_11100001
delay     0b00000000_00000000_00000111_11100010
r 0b00000000_00000000_00000111_11100010
delay     0b00000000_00000000_00000111_11100011
r 0b00000000_00000000_00000111_11100011
delay     0b00000000_00000000_00000111_11100100
r 0b00000000_00000000_00000111_11100100
delay     0b00000000_00000000_00000111_11100101
r 0b00000000_00000000_00000111_11100101
delay     0b00000000_00000000_00000111_11100110
delay     0b00000000_00000000_00000111_11100111
delay     0b00000000_00000000_00000111_11101000
r 0b00000000_00000000_00000111_11101000
r 0b00000000_00000000_00000111_11101000
r 0b00000000_00000000_00000111_11101000
r 0b00000000_00000000_00000111_11101000
delay     0b00000000_00000000_00000111_11101001
r 0b00000000_00000000_00000111_11101001
r 0b00000000_00000000_00000111_11101001
delay     0b00000000_00000000_00000111_11101010
r 0b00000000_00000000_00000111_11101010
r 0b00000000_00000000_00000111_11101010
delay     0b00000000_00000000_00000111_11101011
r 0b00000000_00000000_00000111_11101011
r 0b00000000_00000000_00000111_11101011
r 0b00000000_00000000_00000111_11101011
delay     0b00000000_00000000_00000111_11101100
delay     0b00000000_00000000_00000111_11101101
delay     0b00000000_00000000_00000111_11101110
delay     0b00000000_00000000_00000111_11101111
delay     0b00000000_00000000_00000111_11110000
delay     0b00000000_00000000_00000111_11110001
delay     0b00000000_00000000_00000111_11110010
duration  0b00000000_00000000_00000000_00000111
status    0b00000000_00000000_00000000_10110010
duration  0b00000000_00000000_00000000_00000111
status    0b00000000_00000000_00000000_10110010
duration  0b00000000_00000000_00000000_00000111
status    0b00000000_00000000_00000000_10110010
duration  0b00000000_00000000_00000000_00001000
status    0b00000000_00000000_00000000_10110010
delay     0b00000000_00000000_00000111_11110011
duration  0b00000000_00000000_00000000_00000110
status    0b00000000_00000000_00000000_10110010
duration  0b00000000_00000000_00000000_00000110
status    0b00000000_00000000_00000000_10110010
```

![](../images/attempt004_successes.png)

![](../images/attempt004_wrong_non_zeros.png)

## Bilan
  - (presque) super fun (slow debug !)
  - resultats inesperes
  - procrastination 9999 - 1 Fabien !
