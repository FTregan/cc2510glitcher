# cc2510glitcher

> ⚠️ Until verison 0.1.0 is reached, I may commit code that do not work, I may modify the code so it work with a different pinout or even signal polarity. Do NOT try running previous 0.1.0 code unless you really review everything.
> The code is made available to be studyed, not to be used.

## Project aim

This is a *side project* to see if I could:
- shut the little voice in me that has been whispering *"this is awesome but it is for people way stronger than me"* since I learnt about microcontolers glitching about ten years ago
- see what I will learn in the process (I'm a java developper, I have not practices much assembly since my 486Dx2-66 days)


## What is meant by *glitching* ?

Putting the microcontroler in an out-of-specification state when it is trying to do something, hoping that it will fail and do something different.

To evaluate the success I will try, given a CC2510 with Debug Lock Bit set to 1, to use the debug interface to send a `DEBUG_INSTR` command to execute a single instruction (`mov a, #%01101010`), which should normally prevented.
The `DEBUG_INSTR` command answers on the debug interface with the value of the `a` register, so on success it would answer `%01101010`.


## Documentation

- the [CC2510 Datasheet](https://www.ti.com/lit/ds/symlink/cc2510.pdf?ts=1706377956597&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FCC2510) contains most of the needed and available information in chapter *11 - Debug Interface*.
- the [CC1110/ CC2430/ CC2510 Debug and Programming Interface Specification Rev. 1.2](https://www.ti.com/lit/ug/swra124/swra124.pdf?ts=1715365606009&ref_url=https%253A%252F%252Fwww.google.com%252F) as some additionnal information about the interface and how to use it.
- Jasper Devreker already managed to achieve the glitching and has [this interesting writting](https://zeus.ugent.be/blog/22-23/reverse_engineering_epaper/).
- I later found a recording of this [Glitching the Silicon of the Connected World](https://www.youtube.com/watch?v=CX71p_qcCxY) talk at Black Hat by Thomas Roth, Josh Datko and Dmitry Nedospasov would have been a great introduction to the subject.

