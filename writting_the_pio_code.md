# Writting the pio code to try glitching a cc25010

## choosing the pins

I have already soldered wires form the target board.
If I dont want to reod the solder and wire bending work, I need to have these consecutiv pins affected in this order to:
- ground
- clock
- data
- reset
- power

(I use pins 23-27 of the original raspberry pico pinout - GND then GPIO 18-21)

Since I want to execute two things in parallel, I will need at least two state machine.
One will be responsible for discussing with the cc2510, the other one for sending the glitch trigger signal.

### constraint from pin direction

The only pin that will need to change its direction during operation is `data`.
We can change it from some core (Cortex M0) code, but this would require synchronisation with the state machine and problaby slow things down.
To change a pin direction directly from a PIO instruction we can use:
- an `out` instruction with a `pindirs` destination and `osr` as source using the `out` mapping,
- a `set` instruction with a `pindirs` destination and a 5 bits value as source using the `set` mapping,
- any instruction with a side set with `SIDE_PINDIR` set to `1`, using the `sideset` mapping

Using `osr` for pin direction is really not wanted: we prefer to use `out` instruction which is needed to get data from the core and comes with free autopull and bit counters that save time and memory space.

Do we prefer to use `set` for data direction or for value? Lets compare the code.

Fist with set used for pin directions:

```
next:
    pull block                                          // force pull in case bits are left from last iteration (previous command length not multiple of 32)
    out         x, 32           side 0b0                // read command length, clock low
    out         y, 32                                   // read response length
    wait 1      irq 1                                   // synch with the glitching state machine

write:
    jmp !x      write_end                               // skip write if command length = 0
    set         pindirs, 1                              // set pin to output
    jmp x--     write_next                              // prevent command length off by one
write_next:
    out         pins, 1         side 0b1        [??]    // clock high, output data
    jmp x--     write_next      side 0b0        [??]    // clock low
write_end:

read:
    jmp !y      next                                    // skip write if command length = 0
    set         pindirs, 0                              // set pin to input
    jmp y--     read_first                              // y-- to prevent answer length off by one
read_next:
    in          pins, 1         side 0b0        [??]    // clock low, input data
read_first:
    jmp y--     read_next       side 0b1        [??]    // clock high
read_last:
    in          pins, 1         side 0b0        [??]    // input last bit, clock low

.wrap
```

Out of the 5 bits shared between delay and side set, one is used by side set (two in this code but we could make side set not optionnal, reducing to one), so delay can be encoded on four bits for a total of up to 16 (1 + 15 delay) cycles per instruction.
We know from previous tests that 6 is long enough (12 if we later need to overclock the rp2040 to 240MHz) so this is fine.
Code takes 14 instructions.

When translating this code to use `set` for the direction and side sets for clock, we quickly see that there are two places where pin direction is changed and five where data is changed.
So we save two `set`s but add five, loosing three words of program memory.
Since this does not come with other advantage (code readability ?)

So the mappings used for the communication on the debug interface will be:
- side set: clock pin, 1 bit
- out: data, 1 bit
- set: same as out

For the glitching state machine, we do not need setting pin direction - we only want outputs.
This means the three mappings (out, set and side set) can be used for output.

The more interesting one to use with side set is the glitch trigger, because this will allow to set it during the duration loop and not before, hence making it more easy to have one cycle long signals.

Since we will get the delay before the glitch and its duration from a 32 bits values, they could last more than thirty-five seconds with a one instruction loop without using the delay nor clock divider features of the PIO.
As we dont need the delay, all five bits of the opcode are available for side set.
Even if we want to have optionnal side sets, we can set four consecutiv bits.
This mean we can handle reset, power, glitch signal, and add a fourth signal for oscilloscope synchronisation.
(one thing I learned during my tests is that having pin dedicated to triggering the oscilloscope is a nice analysis tool)
If we handle the reset signal with the state machine, we will need to somehow handle the clock also so we can (re)enter debug mode (entering debug mode is done by pulling `RESET_N` low, then sending twi `CLOCK` signals).
With the current pins mapping, if we want to control the `clock` pin, the three consecutiv pins we can also control are `data`, `reset` and `power` and glitching is not available.
I could desolder the wires on my board to invert `data` and `clock`, but am a bit lazy.

Since I do not want to desolder everything anyway, I will opt for this mappings:
- side set: reset pin, 4 bits (reset, power, glitch and oscilloscope)
- set: clock

This worked until I wanted to handle the delays between `reset_n` and the two `clock` pulses (to enter debug mode) and after the two pulses before starting to communicate on the debug interface with a parameter (which is sent thrue the out fifo).
Then code then becomes too long by ONE instruction (total of 33 words for the two state machines)

I have several solutions to this problems:
- remove some feature of the code (e.g. allowing skipping the glitch by passing a 0 duration, the glitch itself), find the proper values for the two duration and try to find a way to hardcode them instead of passing them as parameters to save space
- use the two PIOs of the rp2040 so my programs (the one for debug/power/glitch and the one for communication on debug interface) have 32 words each instead of sharing one 32 byte slot.
Chapter 3.2.6 seems to give some information about synching with the system IRQ, maybe is it possible to "connect" two different irq of two different PIOs and that's all need to be done?
- try sharing code from the two programs
- stream some instructions or the clock pattern to enter debug mode from the fifo instead of having six instructions (plus 2x2 for delays) just for it
- use a pin for `CLOCK` that could be set with side sets, that mean desoldering again.

Ok, maybe I should really resolder these wires.

side note: the rp2040 is not a real datasheet, closer to an user guide.
as an example, [this question](https://github.com/raspberrypi/pico-feedback/issues/65) is an important one, and even after reading a helpfull answer, I could not find the information in the datasheet.
I still could not find information about how well I could synch the two PIOs.

Ok, I changed the pinout, going from this:

![](images/wiring.jpg)

to this :

![](images/wiring_inverted.jpg)

the beding is cleaner, un fortunately the blue wire is bent to far from the PCB and dives to the other side of the PCB before it left the breadboard, to the wire no longer stay flat.
On the other hand, I added some cardboard so it is easyer to unplug all wires together without bending them.
I could have drown them in cyanoacrylate or epoxy glue, but carboard allower me to replace a wire more easyly if needed.

Now I can update the code and try to add delay before and after the clock signals.

I've not tested the code yet, but I now use 2x15 words, that leave two words to tell the state machine how long to power off the cc2510 before reset!

later: this won't work.
Trying to have these side sets:
```
  ╭──── Glitch
  │╭─── Power
  ││╭── !Reset
  │││╭─ Clock
0b....
```
is a nice idea, but then, even without optionnal side sed, let you only one bit for delaying the instructions (so the PIO can pause for 0 or 1 cycle after each instruction).
But I need longer delays to send the clock signals to enter debug mode:
```
    set         x, 1
clock_pulses:
    nop                         side 0b0101     [7]     // clock high
    jmp x--     clock_pulses    side 0b0100     [7]     // clock low
```
and multiplying the `nop`s looses too much program space.

So, after better thinking:
- I need side set for the glitch only so I can have a one cycle long glitch loop
- I need three bits for delay
- I then only have one bit let that can be used for delay, for `power` (or any other pin adjacent to `glitch`, maybe reordering the wires again is needed), or to mark the side set as optionnal
- There is not enough bits to set clock and reset with side sets, so I will need to use `set`s.
(`out`s could do too, but would require moving data to the OSR which requires either instructions or cooperation with the core)

So lets go back to the first idea of using set + side side and try to make this code fit in the 32 words space.

### 2024-02-10 14:25

I wanted to write documentation, but I making mistake, trying other things…
I decide to try a log+documentation format.

I implemented all pins on PIO (data, clock, reset, power, glitch and oscilloscope synchronisation).
I could add parameterized timing for power off duration, delay before reset and clock, delay between end of reset and start of glitch, and glitch duration.
Unfortunately this is not enough: the delay that `reset` needs to rise is really to long at 10µs.
Delay for `power` rise and fall is even longer at 2ms.

I can :
- move handling of the power back to the core (from PIO), that would save me two delays (power on and power off), and can reuse two of the four instructions to add a reset delay
- try to remove capacitors on the board, hoping durtion for either power on/off or reset on/off can drop to less than 10 cycles and handle them with hardcoded delays

### 2024-02-10 14:47

I moved the code for the power on/off back to the core.
I now have something that works and I can easily test different timings.
With current configuration, it runs very slow, mainly because how long it takes to get the power on/off.
Currently I can do one try every 10ms.
About 95% of the time is spent waiting for the power to lower/rise.
I probably can reduce this a lot by removing that big capacitor between gnd and vcc.

### 2024-05-22

I rewrote most of the code.
One of the reason was I wanted to move all the code I could to the core to save space on the pio which has only 32 instructions space.
I started using the IRQs to synchronize the state machines between themselves and with the core.

I did not move the code to enter debug mode entierely to the core.
One reason is that entering debug requires to manipulate reset and clock lines: reset has moved but reset need to have PIO function to send / receive data and it avoids switching the function when resetting.
The other reason is that the pulse on clock need to have a minimal duration and tining the code is more elegantly done on the PIO than on the core.

This seemed to work, but when I started adding variable commande length I ran out of PIO program space again.
One possibility would be to move the code to the other PIO, saving five words on the first one, and the only drawback seems to be that the PIN needs to be switched to PIO1 than back to PIO0.
It would also allow to move the `reset` signal handling to the same PIO in the same program, so in the end might be cleaner anyway.

Running the tests I found an interesting behaviour: trying to executing some locked command would, as expected, return only `0`s, until at some point it would start returning only `1`s… even for the non locked commands.
The weird thing is that in my outer loop these non locked commands continued to work.

After some investigations I found that… I still had an hardcoded command length of 8 bits.
So *maybe* what happened is that when glitching does not work, the command is just ignored and reading the data line gives a `0` because of some high resistor pull-down on the rp2040 side.
When the glitch works, but I switch too early the data pin to input mode, the cc2510 is stay in input mode waiting for next bits and it possibily has a lower value pullup resistor in this case, making the rp2040 read only `1`s.
This is of course only speculation but would make sense.
in the end of the outer loop I send data on the UART, loosing enough time so the cc2510 debug interface resets and that would explain why the outer loop command still work.
Current task is to allows for variable length commands.

I tried debugging some code: the state machine is stalled and I wanted to inspect the registers to be sure it is on the instruction I think it is.
Using GDB, I eXaminated memory at `0x50200000` + `0xd4` (`PIO0_BASE` + `SM0_ADDR`) to read the instruction pointer of state machine of PIO 0, but could not make sense of the `0x0000001c` value I got.
PIO can not be stepped nor even breakpointed, so learning to read these registers is the only thing I can do beside static analysis of the code.
This I what I try doing this morning.

I also spent some more time understanding the numbers and delays used by the original blog post, and it seems it uses a 1000x slower communication than I do. That may be part of the issue.


### 2024-05-22 learning to debug PIO code

The code stalls early at first command sent via the debug interface: a `GET_CHIP_ID` without glitch attempt that worked before last change.

First thing: GDB confirms the PIO program memory is accessible only via readonly registers to the core:
```
(gdb) x/32wx 0x50200048
0x50200048:     0x00000000      0x00000000      0x00000000      0x00000000
0x50200058:     0x00000000      0x00000000      0x00000000      0x00000000
0x50200068:     0x00000000      0x00000000      0x00000000      0x00000000
0x50200078:     0x00000000      0x00000000      0x00000000      0x00000000
0x50200088:     0x00000000      0x00000000      0x00000000      0x00000000
0x50200098:     0x00000000      0x00000000      0x00000000      0x00000000
0x502000a8:     0x00000000      0x00000000      0x00000000      0x00000000
0x502000b8:     0x00000000      0x00000000      0x00000000      0x00000000
```

Labels might be a way to find if the code is actually at the address I thing it is.

A shorted way was to step through the code and print the result of this line:

```
(gdb) p installed
$6 = rp2040_hal::pio::InstalledProgram<rp2040_pac::PIO0> {
  offset: 9,
  length: 18,
```
Programs are apparently installed at the end of the memory first.
With my current code, installations happen in this order:
- `cc_enter_debug`, length 5, is installed at offset 27
- `cc_debug_command`, length 18, is installed at offset 9
- `cc_glitch_pio`, length 9, is installed at offset 0

The stalled code is from the debug command program, running on State Machine 1 not 0, so instead of examinating `0x502000d4` I Xed `0x502000ec` and found `0x0000000a` (10 decimal).
This should be the second instruction of `cc_debug_command` (installed at offset 9), i.e. `set pindirs, 0b1`, which makes no sense.
But if the previous instuction in stalled and the instruction `SM1_ADDR` has already been incremented, then currently stalled instruction would be the previous one: `irq wait 1`, which is more likely.

The IRQ is there to synchronize with the state code sending the glitch signal running on State Machine 2.
This one is stalled at address 0, which is an `out x, 32` (reading from the Tx fifo).
My first guess was that since I was not sending anything to the fifo since there is no glitch attemps, but this is not the case.
I'll try stepping trough the code.

When the debug program is installed, the program counter points to the first instruction (value 9).
As soon as it is started, the PC is incremented (to 10) even though the instruction in stalled.
That is… if the stalled instruction is a `wait` or an `ira wait` because if it is an `out` the instruction address is not increment.
At least I hope this is the probleme and not some weird bug like stalling an instruction at address zero does not work ^^.
I could not find any mention of either in the datasheet.
You can't call this document a datasheet.

Displaying the the instructino addresses of the three used State Machines (and the Status register of the PIO) while stepping through the rust code I find:
- Sending the glitch delay on the glitch tx fifo advances the instruciton to the second out
- Sending the glitch duration on the same fifo unstalls the send command SM until instruction 14 (which gets a bit fromn the fifo to send a bit of data to the data pin)

Here is the GDB output:

```
Breakpoint 7, cc2510glitcher::cc2510::Cc2510<rp2040_hal::gpio::pin::bank0::Gpio20, rp2040_hal::gpio::pin::bank0::Gpio21>::send_with_glitch<rp2040_hal::gpio::pin::bank0::Gpio20, rp2040_hal::gp
io::pin::bank0::Gpio21> (self=0x2003fe90, c=..., delay=0, duration=0) at src/cc2510.rs:118
3: *(0x502000d4 as *u8) = 28
4: *(0x502000ec as *u8) = 10
5: *(0x50200104 as *u8) = 0
7: /t *(0x50200004 as *u32) = 1111000000000000111100000000
(gdb) n
3: *(0x502000d4 as *u8) = 28
4: *(0x502000ec as *u8) = 10
5: *(0x50200104 as *u8) = 1
7: /t *(0x50200004 as *u32) = 1111000000000000111100000000
3: *(0x502000d4 as *u8) = 28
4: *(0x502000ec as *u8) = 14
5: *(0x50200104 as *u8) = 0
7: /t *(0x50200004 as *u32) = 1111000000000000111100000000
(gdb)
```

this is a problem: at least while stepping, the code of the `cc_send_program` program in unstalled before core could write to the fifo.
I have to read the dreaded datasheet again.

Indeed, I moved a `pull block` instruction for code readability that should not have been moved.
This at least shows I can somehow use gdb to find some PIO code bugs.
